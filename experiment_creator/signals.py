from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from actstream import action
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_comments.signals import comment_was_posted
from django_comments_xtd.models import XtdComment, TmpXtdComment

from .models import StimuliDesign, ExperimentStimuli
from .util.spr_lists import generate_lists_for_experiment


@receiver(post_save, sender=StimuliDesign)
def proposal_save_handler(sender, instance, created, raw, using, update_fields, *args, **kwargs):
    """
    Add an ActStream notification when a design proposal is saved.

    example from docs:
    justquick (actor) closed (verb) issue 2 (object) on django-activity-stream (target) 12 hours ago
    """
    if created:
        action.send(instance, verb='was created')
    if update_fields:
        if 'status' in update_fields:
            verb = {
                'NEW': 'was created',
                'REQ': 'was submitted for approval',
                'APP': 'was approved',
                'REV': 'needs to be revised',
                'RUP': 'was revised'
            }[instance.status]
            action.send(instance, verb=verb)
            if instance.status == 'APP':
                instance.experiment.status = 'STM'
                instance.experiment.save()
        else:
            action.send(instance, verb='was saved')
            action.data['updated'] = update_fields

        if 'design' in update_fields:
            instance.validate()


@receiver(post_save, sender=ExperimentStimuli)
def stimuli_save_handler(sender, instance, created, raw, using, update_fields, *args, **kwargs):
    """Generate SPR files when stimuli are approved"""
    if update_fields:
        if 'status' in update_fields:
            if instance.status == 'APP':
                generate_lists_for_experiment(instance.experiment)

                instance.experiment.status = 'GTS'
                instance.experiment.save()
                action.send(instance.experiment, verb='ready for testing by group')


@receiver(comment_was_posted, sender=TmpXtdComment)
def posted_comment_handler(sender, comment, request, **kwargs):
    if comment.level == 0:
        action.send(comment.user, verb='commented on', target=comment.content_object)
    else:
        action.send(comment.user,
                    verb='replied to',
                    action_object=XtdComment.objects.get(id=comment.parent_id),
                    target=comment.content_object)
