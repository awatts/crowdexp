from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from os.path import splitext

from django.contrib.auth.models import User
from django.forms import Form, ModelForm, ValidationError, Textarea, CharField, CheckboxSelectMultiple, PasswordInput
from django.forms.fields import MultipleChoiceField
from django_comments_xtd.forms import XtdCommentForm

from .models import WordDocument, ExcelDocument, Course, CourseGroup


def _format_student(student):
    """
    Format student name for display in forms.
    """
    groupnames = ''
    if student.course_groups.count() > 0:
        groupnames = ' (Member of: ' + ', '.join([g.name for g in student.course_groups.all()]) + ')'
    return '{} ({}) <{}> {}'.format(student.profile.get_list_name(), student.username, student.email, groupnames)


class WordForm(ModelForm):
    class Meta:
        model = WordDocument
        fields = ('file', 'description')
        labels = {
            'file': 'MS Word (.doc, .docx) file',
            'description': 'File description (optional)'
        }
        widgets = {
            'description': Textarea(attrs={'cols': 20, 'rows': 5}),
        }

    def clean_file(self):
        data = self.cleaned_data['file']
        if not data:
            raise ValidationError('You must upload a file!')
        ext = splitext(data.name)[1].lower()
        if ext not in ('.docx', '.doc'):
            print('Got a bad document')
            raise ValidationError('You can only upload MS Word files')
        return data


class ExcelForm(ModelForm):
    class Meta:
        model = ExcelDocument
        fields = ('file', 'description')
        labels = {
            'file': 'MS Excel (.xls, .xlsx) file',
            'description': 'File description (optional)'
        }
        widgets = {
            'description': Textarea(attrs={'cols': 20, 'rows': 5}),
        }

    def clean_file(self):
        data = self.cleaned_data['file']
        if not data:
            raise ValidationError('You must upload a file!')
        ext = splitext(data.name)[1].lower()
        if ext not in ('.xlsx', '.xls'):
            raise ValidationError('You can only upload MS Excel files')
        return data


class CourseJoinForm(Form):
    courses = MultipleChoiceField(
        choices=[(c.id, '{} ({} {})'.format(c.name, c.term, c.year)) for c in Course.objects.filter(current=True)],
        widget=CheckboxSelectMultiple,
        required=False
    )


class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = ('year', 'term', 'name', 'current', 'students', 'instructors')
        widgets = {
            'students': CheckboxSelectMultiple,
            'instructors': CheckboxSelectMultiple
        }

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['students'].choices = [(m.id, '{} ({}) <{}>'.format(
            m.profile.get_list_name(), m.username, m.email)) for m in self.fields['students'].queryset]
        self.fields['instructors'].choices = [(m.id, '{} ({}) <{}>'.format(
            m.profile.get_list_name(), m.username, m.email)) for m in self.fields['instructors'].queryset]


class CourseGroupForm(ModelForm):
    class Meta:
        model = CourseGroup
        fields = ('name', 'students')
        labels = {
            'name': 'Group Name',
            'students': 'Students'
        }
        widgets = {
            'students': CheckboxSelectMultiple
        }

    def __init__(self, *args, **kwargs):
        course_id = kwargs.pop('course_id', None)
        students = CourseGroup.objects.none()  # empty QuerySet just in case 'group_id' isn't specified
        if course_id:
            course = Course.objects.get(id=course_id)
            students = course.students.order_by('last_name', 'first_name')
        super(CourseGroupForm, self).__init__(*args, **kwargs)
        self.fields['students'].choices = [(m.id, _format_student(m)) for m in students]
        self.fields['students'].queryset = students



class CourseGroupJoinForm(Form):
    joining_pin = CharField(
        label='Joining PIN',
        required=True,
        widget=PasswordInput(
            attrs={
                'placeholder': 'Ask a group member if you need the PIN',
                'minlength': 6,
                'maxlength': 6
            }
        )
    )

    def clean(self):
        self.cleaned_data = super(CourseGroupJoinForm, self).clean()

        joining_pin = self.cleaned_data['joining_pin']
        group_pin = self.data['group'].joining_pin
        if joining_pin:
            # Only try to check PINs if it passed validation at super level
            if joining_pin != group_pin:
                raise ValidationError('Incorrect PIN!')


class CourseGroupExitForm(Form):
    joining_pin = CharField(
        label='Group PIN',
        required=True,
        widget=PasswordInput(
            attrs={
                'placeholder': 'Copy the PIN from above to confirm',
                'minlength': 6,
                'maxlength': 6
            }
        )
    )

    def clean(self):
        self.cleaned_data = super(CourseGroupExitForm, self).clean()

        joining_pin = self.cleaned_data['joining_pin']
        group_pin = self.data['group'].joining_pin
        if joining_pin:
            # Only try to check PINs if it passed validation at super level
            if joining_pin != group_pin:
                raise ValidationError('Incorrect PIN!')



class GroupMemberForm(Form):
    """Form for selecting from members of a CourseGroup."""
    def __init__(self, *args, **kwargs):
        group_id = kwargs.pop('group_id', None)
        students = CourseGroup.objects.none()  # empty QuerySet just in case 'group_id' isn't specified
        if group_id:
            group = CourseGroup.objects.get(id=group_id)
            students = group.students.order_by('last_name', 'first_name')
        super(GroupMemberForm, self).__init__(*args, **kwargs)
        self.fields['members'].choices = [(m.id, _format_student(m)) for m in students]
        self.fields['members'].queryset = students

    members = MultipleChoiceField(
        widget=CheckboxSelectMultiple,
        required=False
    )


class GroupNonMemberForm(Form):
    """Form for selecting from members of Course who are not members of a speficied CourseGroup."""
    def __init__(self, *args, **kwargs):
        group_id = kwargs.pop('group_id', None)
        students = CourseGroup.objects.none()  # empty QuerySet just in case 'group_id' isn't specified
        if group_id:
            group = CourseGroup.objects.get(id=group_id)
            group_ids = [s.id for s in group.students.all()]
            students = group.course.students.exclude(id__in=group_ids)
        super(GroupNonMemberForm, self).__init__(*args, **kwargs)
        self.fields['non_members'].choices = [(m.id, _format_student(m)) for m in students]
        self.fields['non_members'].queryset = students

    non_members = MultipleChoiceField(
        widget=CheckboxSelectMultiple,
        required=False
    )


class CourseNonStudentsForm(Form):
    """Form to add students to a course"""
    def __init__(self, *args, **kwargs):
        course_id = kwargs.pop('course_id', None)
        students = Course.objects.none() # empty QuerySet just in case 'course_id' isn't specified
        if course_id:
            course = Course.objects.get(id=course_id)
            students = User.objects.exclude(enrolled_courses=course).filter(is_staff=False).order_by('last_name', 'first_name')
        super(CourseNonStudentsForm, self).__init__(*args, **kwargs)
        self.fields['non_members'].choices = [(m.id, _format_student(m)) for m in students]
        self.fields['non_members'].queryset = students

    non_members = MultipleChoiceField(
        widget=CheckboxSelectMultiple,
        required=False
    )


class CourseStudentsForm(Form):
    """Form to remove students to a course"""
    def __init__(self, *args, **kwargs):
        course_id = kwargs.pop('course_id', None)
        students = Course.objects.none() # empty QuerySet just in case 'course_id' isn't specified
        if course_id:
            course = Course.objects.get(id=course_id)
            students = course.students.order_by('last_name', 'first_name')
        super(CourseStudentsForm, self).__init__(*args, **kwargs)
        self.fields['members'].choices = [(m.id, _format_student(m)) for m in students]
        self.fields['members'].queryset = students

    members = MultipleChoiceField(
        widget=CheckboxSelectMultiple,
        required=False
    )


class EmailGroupForm(Form):
    subject = CharField()
    body = CharField(widget=Textarea)

