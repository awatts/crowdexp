# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-18 15:31
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('experiment_creator', '0008_stimulidesign_proposal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stimulidesign',
            name='proposal',
            field=ckeditor.fields.RichTextField(default='<p>Describe your proposed experiment here.</p>'),
        ),
    ]
