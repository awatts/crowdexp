from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from os import makedirs, mkdir
import os.path
from random import shuffle
from random import randint
from itertools import chain
from copy import deepcopy
from operator import itemgetter
from typing import List, Dict, Tuple
import pandas as pd
from django.conf import settings
from ..models import Experiment
from .tagged_spr_parser import Region, parse_tagged_sentence
from experiment_runner.models import Experiment as RunnerExperiment
from experiment_runner.models import TrialList


def wrap_inc(num: int, max:int = 4) -> int:
    """Returns next number in sequence [1..max], looping at max."""
    if num == max:
        return 1
    else:
        if num > max:
            return num % max + 1
        else:
            return num + 1


def format_region(region: Region) -> str:
    """Take a Region namedtuple and return a 'region:start,stop' string."""
    if isinstance(region, list):
        return ''
    else:
        return "{}:{},{}".format(region.tag, region.start, region.stop)


def generate_config_file(listname: str) -> str:
    """Output the generic Linger-esque config file for a given list"""
    config_template = """set	randomize	false\
\nset	insertBreaks	true\
\ninstruct	Thank you for taking part in our study!\
\ninstruct	In this task you'll read simple sentences and answer yes/no questions about them.<br/>Instead of seeing a whole sentence straight away, you will just see a word at a time. You can read the sentence at your own pace by pressing the space bar when you are ready to see the next word.\
\ninstruct	To start with, all you will see is a horizontal line. This line has a sentence "hidden" behind it.<br/><br/>To read the sentence, you will have to press the space bar. This will make the line disappear piece by piece, so that you can see the hidden words.<br/><br/>Every time a new word is revealed, the previous word will be hidden again. There is no way to return to words you have already read once they are hidden, so please read carefully.\
\ninstruct	Please read silently, not aloud. Once you have read each sentence, we will ask you question about it.<br/><br/>When you're ready to try some practice sentences, click "OK".	So far so good? Y\
\npractice	This sentence is a practice sentence to help you get used to a moving window display.	So far so good?	Y\
\npractice	This sentence is also for practice, and is followed by a question.	Are you following this so far?	Y\
\npractice	Now you will get to see what happens if you answer a question incorrectly.	Press the "yes" button now, ok?	N\
\ninstruct	OK, that's the end of the instructions! You'll get used to it quickly.<br/><br/>When you click OK, you can start reading the first "real" sentence. Please read carefully, and do your best to answer the questions correctly!<br/><br/>We will not be able to pay for HITs that are submitted incomplete or with more than a quarter of questions answered incorrectly.<br/><br/>Good luck!\
\nitems	{}.itm\
\nend	Thanks for participating. Please take a moment to give us feedback on this experiment in the box below. Then click on the "Submit" button below to complete the HIT and return to Mechanical Turk. Goodbye!"""
    return config_template.format(listname)


def create_lists(experiment_name: str) -> Tuple[List[Dict[str, str]], pd.DataFrame]:
    """Create Linger-esque item files from lists in an Excel file"""
    experiment = Experiment.objects.get(name=experiment_name)

    # grab the most recent version of the stimulus file
    excel_file = experiment.folder.children.get(name='Stimuli').files.last()

    df = pd.read_excel(excel_file.file)

    # get all unique conditions except the '-' filler condition
    conditions = set(df['Condition'].unique().tolist()) - set('-')
    numbers_to_conds = dict(zip(range(1, len(conditions) + 1), conditions))
    conds_to_numbers = dict(zip(conditions, range(1, len(conditions) + 1)))

    # get fillers.
    fillers = df[['ItemName', 'Experiment', 'Sentence', 'Condition', 'Question', 'Answer']].where(df['Experiment'] == 'filler').dropna()
    fillers['ItemName'] = fillers.ItemName.astype(int)

    # get critical items and their numbers
    expt_items = df[['ItemName', 'Experiment', 'Sentence', 'Condition', 'Question', 'Answer']].where(df['Experiment'] != 'filler').dropna()
    expt_items['ItemName'] = expt_items.ItemName.astype(int)

    # If there were a destructuring way of doing  this in one whack, I'd prefer it
    parsed_expt_sentences = expt_items['Sentence'].apply(parse_tagged_sentence)
    expt_items['Regions'] = parsed_expt_sentences.apply(lambda x: x.tags)
    expt_items['WordList'] = parsed_expt_sentences.apply(lambda x: x.wordlist)
    expt_items['Parsed'] = parsed_expt_sentences.apply(lambda x: x.parsed)

    # If there were a destructuring way of doing  this in one whack, I'd prefer it
    parsed_filler_sentences = fillers['Sentence'].apply(parse_tagged_sentence)
    fillers['Regions'] = parsed_filler_sentences.apply(lambda x: x.tags)
    fillers['WordList'] = parsed_filler_sentences.apply(lambda x: x.wordlist)
    fillers['Parsed'] = parsed_filler_sentences.apply(lambda x: x.parsed)

    regions = pd.concat([expt_items.loc[:, ['ItemName', 'Regions']], fillers.loc[:, ['ItemName', 'Regions']]])

    # Now we want the fillers as a list of dicts
    filler_dicts = fillers.to_dict(orient='records')

    # gotta remember what this is about
    expt_len = int(len(expt_items) / len(conditions))
    square_indices = {numbers_to_conds[i + 1]: range(1, expt_len + 1)[i::len(conditions)] for i in range(len(conditions))}

    # Make a dictionary keyed on condition with item lists
    cond_lists = {cond: expt_items.loc[expt_items['Condition'] == cond].to_dict(orient='records') for cond in conditions}

    # Generate one list of experimental items
    list_square = []
    for c in conditions:
        list_square.extend([x for x in cond_lists[c] if x['ItemName'] in square_indices[c]])

    # XXX: this is equivalent, yes? HAHAHA. No. Doesn't flatten.
    # list_square = [[x for x in cond_lists[c] if x['ItemName'] in square_indices[c]] for c in conditions]

    # Shuffle the items before we generate the final list
    shuffle(list_square)
    shuffle(filler_dicts)

    # Start filler insertion by putting them every other item
    list_square = list(chain.from_iterable(zip(list_square, filler_dicts[:expt_len])))

    if len(filler_dicts) > expt_len:
        # Insert a filler at the beginning and end of the list
        list_square.insert(0, filler_dicts[expt_len])

        # Then randomly insert fillers until we're out
        for f in filler_dicts[expt_len + 1:]:
            while 1:
                insert_idx = randint(0, len(list_square) - 1)
                # take a look at a slice 2 back and forward and make sure
                # there aren't 3 or more fillers
                if [x['Experiment'] for x in list_square[insert_idx - 1:insert_idx + 3]].count('filler') < 3:
                    list_square.insert(insert_idx, f)
                    break

    # Find the indices of each of the experimental items for each condition
    item_conds = [(x['ItemName'], x['Condition']) for x in list_square]
    cond_indices = {c: [dict(zip(('Index', 'ItemName'), (i, x[0]))) for i, x in enumerate(item_conds) if x[1] == c] for c in conditions}

    # Start off our final lists by making the first one a copy of the base list
    final_lists = [deepcopy(list_square)]

    oldlist = list_square
    for j in range(len(numbers_to_conds) - 1):
        newlist = deepcopy(oldlist)
        for k, v in cond_indices.items():
            new_cond = numbers_to_conds[wrap_inc(conds_to_numbers[k] + j, len(conditions))]
            items = sorted(v, key=itemgetter('Index'))

            replacements = [x for x in cond_lists[new_cond] if x['ItemName'] in [y['ItemName'] for y in items]]
            for i in items:
                idx, itm_name = i['Index'], i['ItemName']
                # Gets the matching item or None (should always match)
                # XXX: this is really inefficient; searches whole list every time
                newlist[idx] = next((x for x in replacements if x['ItemName'] == itm_name), None)
        final_lists.append(newlist)
        oldlist = newlist

    # create reversed lists by mapping reverse over existing and then appending result?
    rev_lists = [list(x) for x in map(reversed, final_lists)]

    # And then add the reversed lists to the final lists
    final_lists.extend(rev_lists)

    # Make a list of dicts with listname and contents
    outdata = [{'sheet': 'list{}'.format(i), 'contents': stimlist} for i, stimlist in enumerate(final_lists, 1)]

    return outdata, regions


def write_spr_files(experiment_name: str, dirname: str, basepath: str) -> List[str]:
    """
    Given a list of dicts with sheetnames as keys, where each dict has as its
    value a list of dicts with values of each row, output a directory of
    Linger-style stimuli files, one per dict
    """

    # create directory with dirname
    fullpath = os.path.join(basepath, dirname)
    if not os.path.exists(fullpath):
        makedirs(fullpath)

    sprdata, regions = create_lists(experiment_name)

    list_names = []
    for row in sprdata:
        sheet = row['sheet']
        contents = row['contents']

        list_names.append(sheet)
        sprdir = os.path.join(fullpath, sheet)
        if not os.path.exists(sprdir):
            mkdir(sprdir)

        with open(os.path.join(sprdir, "{}.itm".format(sheet)), 'w') as sprfile:
            for item in contents:
                print("# {} {} {}".format(item['Experiment'], item['ItemName'], item['Condition']), file=sprfile)
                # To get all questions (or answers, etc.). returns list of tuples
                sentences = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Parsed")],
                    key=itemgetter(0))
                for sent in sentences:
                    print(sent[1], file=sprfile)

                questions = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Question")],
                    key=itemgetter(0))
                answers = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Answer")],
                    key=itemgetter(0))
                for quest, ans in zip([q[1] for q in questions], [a[1] for a in answers]):
                    print("? {} {}".format(quest, ans), file=sprfile)

        with open(os.path.join(sprdir, 'config'), 'w') as configfile:
            print(generate_config_file(sheet), file=configfile)

        regions.to_pickle(os.path.join(sprdir, 'regions.pickle'))

    return list_names


def generate_lists_for_experiment(experiment: Experiment) -> None:
    """Generate trial list files and TrialList objects for an Experiment."""
    runner_experiment, _ = RunnerExperiment.objects.get_or_create(name=experiment.slug)

    flexspr = os.path.join(settings.STATIC_ROOT, 'flexspr')
    if not os.path.exists(flexspr):
        mkdir(flexspr)

    list_names = write_spr_files(experiment.name, experiment.slug, flexspr)
    for lname in list_names:
        TrialList.objects.get_or_create(name=lname, experiment=runner_experiment)
