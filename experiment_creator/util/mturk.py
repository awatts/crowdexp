from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
from datetime import timedelta
# from pprint import pprint
from typing import List

from boto.mturk.connection import MTurkConnection, MTurkRequestError
from boto.mturk.price import Price
from boto.mturk.qualification import LocaleRequirement, PercentAssignmentsApprovedRequirement, Qualifications
from boto.mturk.question import ExternalQuestion
from django.conf import settings

from ..models import Experiment, MTurkHIT

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'SQS_QUEUE_URL': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

os.environ['AWS_DEFAULT_REGION'] = AWS_SETTINGS['SQS_REGION']
os.environ['AWS_ACCESS_KEY_ID'] = AWS_SETTINGS['AWS_ACCESS_KEY']
os.environ['AWS_SECRET_ACCESS_KEY'] = AWS_SETTINGS['AWS_SECRET_KEY']

logger = logging.getLogger(__name__)


def _get_mturk_connection(sandbox: bool=False) -> MTurkConnection:
    """Gets an MTurkConnection, possibly to sandbox"""
    host = 'mechanicalturk.sandbox.amazonaws.com' if sandbox else 'mechanicalturk.amazonaws.com'
    return MTurkConnection(aws_access_key_id=AWS_SETTINGS['AWS_ACCESS_KEY'],
                           aws_secret_access_key=AWS_SETTINGS['AWS_SECRET_KEY'],
                           host=host)


def run_mturk_spr_experiment(experiment: Experiment, experiment_url: str, sandbox: bool=False) -> int:
    """Run SPR MTurk External Question HIT"""
    duration = int(experiment.stimulidesign.design.get('duration', 0))
    assignments = 1 if sandbox else (experiment.stimulidesign.design.get('participants', 0))

    qualifications = Qualifications((
        PercentAssignmentsApprovedRequirement('GreaterThan', 95, True),
        LocaleRequirement('EqualTo', 'US', True)
    ))

    title = 'Self paced reading'
    description = 'Read simple sentences and answer yes/no questions about them.'
    keywords = ['english', 'language', 'reading', 'psychology', 'experiment']

    reward = Price(duration / 60 * 6)  # $6/hr
    assignmentduration = timedelta(minutes=duration * 1.5)  # `duration` minutes and then give them time and a half to do it

    hitlifetime = timedelta(days=3)  # Make the HIT available for 3 days
    autoapprovaldelay = timedelta(days=15)  # Automatically approve any outstanding HITs after 15 days

    url_params = '?dest=sandbox' if sandbox else '?dest=mturk'

    question = ExternalQuestion(experiment_url + url_params, 680)

    # For debugging purposes
    # hit_values = {
    #     'question': question.external_url,
    #     'max_assignments': assignments,
    #     'title': title,
    #     'description': description,
    #     'keywords': keywords,
    #     'duration': assignmentduration.total_seconds(),
    #     'lifetime': hitlifetime.total_seconds(),
    #     'approval_delay': autoapprovaldelay.total_seconds(),
    #     'reward': reward.get_as_params('reward'),
    #     'qualifications': qualifications.get_as_params(),
    #     'sandbox': sandbox
    # }
    #
    # pprint(hit_values)

    mtc = _get_mturk_connection(sandbox=sandbox)

    try:
        created_hit = mtc.create_hit(question=question,
                                     max_assignments=assignments,
                                     title=title,
                                     description=description,
                                     keywords=keywords,
                                     duration=assignmentduration,
                                     lifetime=hitlifetime,
                                     approval_delay=autoapprovaldelay,
                                     reward=reward,
                                     qualifications=qualifications,
                                     response_groups=["Request", "Minimal", "HITDetail", "HITAssignmentSummary"])
    except MTurkRequestError as e:
        logger.error(e.message)
        created_hit = []

    # sqs = boto3.resource('sqs')
    # try:
    #     queue = sqs.get_queue_by_name(QueueName=AWS_SETTINGS['SQS_QUEUE'])  # XXX: returns wrong queue URL
    # except ClientError:
    #     queue = None

    for hit in created_hit:
            try:
                hit_detail = mtc.get_hit(hit.HITId, response_groups=["Request", "Minimal", "HITDetail", "HITAssignmentSummary"])[0]
            except IndexError:
                hit_detail = hit
            new_hit = MTurkHIT(experiment=experiment, props=hit_detail.__dict__, sandbox=sandbox)
            new_hit.save()
            try:
                mtc.set_sqs_notification(hit.HITTypeId, AWS_SETTINGS['SQS_QUEUE_URL'],
                                         ("AssignmentAccepted", "AssignmentAbandoned",
                                          "AssignmentReturned", "AssignmentSubmitted",
                                          "HITReviewable", "HITExpired"))
            except MTurkRequestError as e:
                logger.error(e)

    return len(created_hit)


def update_hit_info(hits: List[MTurkHIT], sandbox: bool=False) -> None:
    """Update HIT information for MTurkHITs"""

    mtc = _get_mturk_connection(sandbox=sandbox)

    for h in hits:
        try:
            hit = mtc.get_hit(h.props['HITId'], response_groups=['Request', 'Minimal', 'HITDetail', 'HITAssignmentSummary'])[0]
            h.props.update(hit.__dict__)
            h.save()
        except IndexError:
            pass
