from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import logging
from smtplib import SMTPDataError, SMTPConnectError, SMTPHeloError, SMTPAuthenticationError
from smtplib import SMTPException, SMTPSenderRefused, SMTPRecipientsRefused
from typing import List, Optional

from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail

from experiment_creator.models import Course, CourseGroup

logger = logging.getLogger(__name__)


def email_course_staff(course_id: int, subject: str, message: str) -> Optional[str]:
    """Send an email to course staff to notify about an event of interest."""
    course = Course.objects.get(id=course_id)
    addresses = [u.email for u in course.instructors.all()]
    return _send_group_email(addresses, subject, message)


def email_course_group(group_id: int, subject: str, message: str) -> Optional[str]:
    """Send an email to course staff to notify about an event of interest."""
    group = CourseGroup.objects.get(id=group_id)
    addresses = [u.email for u in group.get_members()]
    return _send_group_email(addresses, subject, message)


def email_mturk_runners(subject: str, message: str) -> Optional[str]:
    """Email staff members who actually can run experiments on MTurk"""
    staff = User.objects.filter(is_staff=True).all()
    experiment_runners = [u.email for u in staff if u.has_perm('can_run_experiments')]
    return _send_group_email(experiment_runners, subject, message)


def _send_group_email(addresses: List[str], subject: str, message: str) -> Optional[str]:
    """Do the actual work of sending the emails"""
    try:
        mail_user = settings.DEFAULT_FROM_EMAIL
    except AttributeError:
        mail_user = 'admin@localhost'
    try:
        send_mail(
            subject,
            message,
            mail_user,
            addresses,
            fail_silently=False,
        )
    except (SMTPSenderRefused, SMTPRecipientsRefused, SMTPDataError, SMTPConnectError,
            SMTPHeloError, SMTPAuthenticationError, SMTPException) as err:
        logger.error('Failed to send email. %s', err)
        return str(err)
