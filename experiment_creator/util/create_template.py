#!/usr/bin/env python3

from itertools import product
from typing import List
from openpyxl import Workbook
from openpyxl.styles import Font
from openpyxl.utils.cell import get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation

Manipulation = List[str]


def create_xlsx_template(experiment: str, itemcount: int, fillercount: int, conditions: List[Manipulation]) -> Workbook:
    """
    Create an xlsx template for a specific experiment.

    Conditions can be an arbitrarily long sequence of sequences, representing
    the conditions being crossed in the experiment e.g.
    N [['foo', 'bar', ...]]
    NxM [['foo', 'bar', ...], ['baz', 'qux',...]]
    NxMxP [['foo', 'bar', ...], ['baz', 'qux',...], ['zub', 'wibble',...]]

    :param experiment: name of the experiment
    :type experiment: str or unicode
    :param itemcount: number of experimental items
    :type itemcount: int
    :param conditions: list of manipulations
    :type conditions: seq of seqs
    :returns: FIXME: should return path of created file
    :rtype: None FIXME: should be string
    """

    wb = Workbook()
    ws = wb.active

    headings = ('Experiment', 'ItemName', 'Condition', 'Sentence', 'Question', 'Answer')

    # Don't let them put anything but Y or N in the 'Answer' column
    dv = DataValidation(type="list", formula1='"Y,N"', allow_blank=True)
    dv.error = 'Only the values "Y" and "N" can be entered'
    dv.errorTitle = 'Invalid Entry'
    dv.prompt = 'Please select "Y" or "N"'
    dv.promptTitle = 'List Selection'
    ws.add_data_validation(dv)

    # Add header row and bold those cells
    for colx, heading in enumerate(headings, 1):
        col = get_column_letter(colx)
        ws['{}1'.format(col)] = heading
        ws['{}1'.format(col)].font = Font(bold=True)

    rowx = 1
    condnames = ['.'.join(x) for x in product(*conditions)]
    for row in range(1, itemcount + 1):
        for cond in condnames:
            rowx += 1
            for colx, heading in enumerate(headings, 1):
                col = get_column_letter(colx)
                ws['{}{}'.format(col, rowx)] = {
                    'Experiment': experiment,
                    'ItemName': row,
                    'Condition': cond,
                    'Sentence': '', 'Question': '', 'Answer': ''
                }[heading]
                if heading == 'Answer':
                    dv.add(ws['{}{}'.format(col, rowx)])

    # Add 2*itemcount filler rows to remind them of the minimum number
    for row in range(itemcount + 1, itemcount + fillercount + 1):
        rowx += 1
        ws['A{}'.format(rowx)] = 'filler'
        ws['B{}'.format(rowx)] = row
        ws['C{}'.format(rowx)] = '-'
        dv.add(ws['F{}'.format(rowx)])

    # Freezing with openpyxl is weird; freezes everything above and to left of
    # specified cell, so to do top row, must say B2
    c = ws['B2']
    ws.freeze_panes = c

    return wb

    # wb.save(filename='{}.xlsx'.format(experiment))


if __name__ == '__main__':
    # Fake data to demonstrate
    experiment = 'test'
    conditions = [['a', 'b'], ['x', 'y']]

    create_xlsx_template(experiment, 16, 32, conditions)
