from django.apps import AppConfig


class ExperimentCreatorAppConfig(AppConfig):
    name = 'experiment_creator'

    def ready(self):
        from actstream import registry
        from django.contrib.auth.models import User
        from django_comments_xtd.models import XtdComment
        import experiment_creator.signals
        from django_comments.signals import comment_was_posted
        registry.register(self.get_model('Course'))
        registry.register(self.get_model('CourseGroup'))
        registry.register(self.get_model('Project'))
        registry.register(self.get_model('Experiment'))
        registry.register(self.get_model('StimuliDesign'))
        registry.register(self.get_model('ExperimentStimuli'))
        registry.register(self.get_model('WordDocument'))
        registry.register(self.get_model('ExcelDocument'))
        registry.register(User)
        registry.register(XtdComment)
