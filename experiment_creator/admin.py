from django.contrib import admin
from filer.admin.fileadmin import FileAdmin
from ckeditor.widgets import CKEditorWidget
from .models import *


class CourseAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'year', 'term')


class CourseGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'course')


class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('group', 'name',)}
    list_display = ('name', 'slug', 'group', 'active')


class ExperimentAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('group', 'name',)}
    list_display = ('name', 'group', 'project', 'folder')


class StimuliDesignAdmin(admin.ModelAdmin):
    list_display = ('group', 'experiment', 'status')


class ExperimentStimuliAdmin(admin.ModelAdmin):
    list_display = ('experiment', 'group')

admin.site.register(Course, CourseAdmin)
admin.site.register(CourseGroup, CourseGroupAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Experiment, ExperimentAdmin)
admin.site.register(StimuliDesign, StimuliDesignAdmin)
admin.site.register(ExperimentStimuli, ExperimentStimuliAdmin)
admin.site.register(WordDocument, FileAdmin)
admin.site.register(ExcelDocument, FileAdmin)
