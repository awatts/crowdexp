from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function
from filer.fields.file import FilerFileField
from .models import WordDocument, ExcelDocument


class FilerWordField(FilerFileField):
    default_model_class = WordDocument


class FilerExcelField(FilerFileField):
    default_model_class = ExcelDocument
