from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import url

from .views import student, staff, common

urlpatterns = [
    url(r'^landing/$', student.landing, name='landing'),
    url(r'^dashboard/$', student.dashboard, name='dashboard'),
    url(r'^user/(?P<user_id>[0-9]+)$', common.user_profile, name='user_profile'),
    url(r'^courses/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<slug>[A-Za-z0-9-_]+)/$',
        student.course_dash, name='course_dash'),
    url(r'^courses/join/(?P<course_id>[0-9]+)/$', student.join_course, name='join_course'),
    url(r'^courses/manage_membership/$', student.manage_course_membership, name='manage_course_membership'),
    url(r'^project/(?P<project_id>[0-9]+)/$', student.project_dash, name='project_dash'),
    url(r"^group/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/(?P<group_id>[0-9]+)/$",
        student.group_dash, name='group_dash'),
    url(r'^group/create/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/$',
        common.create_group, name='create_group'),
    url(r'^group/join/(?P<group_id>[0-9]+)/$', student.join_group, name='join_group'),
    url(r'^group/leave/(?P<group_id>[0-9]+)/$', student.leave_group, name='leave_group'),
    url(r'group/request_membership/(?P<group_id>[0-9]+)/$', student.request_group_membership, name='request_group_membership'),
    url(r'group/request_leaving/(?P<group_id>[0-9]+)/$', student.request_group_leaving, name='request_group_leaving'),
    # Design
    url(r"^design/create/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/(?P<group_id>[0-9]+)/(?P<slug>[A-Za-z0-9-_]+)/$", student.create_design, name='create_design'),
    url(r'^design/get_set/(?P<experiment_id>[0-9]+)/$', student.get_set_design, name='get_set_design'),
    url(r'^design/requestapproval/(?P<experiment_id>[0-9]+)/$', student.request_design_approval, name='request_design_approval'),
    url(r'^design/download_excel/(?P<experiment_id>[0-9]+)/$', student.download_excel_template, name='download_excel_template'),
    url(r'^design/add_feedback/(?P<target_id>[0-9]+)/$', common.add_feedback, {'feedback_type': 'design'}, name='design_feedback'),
    url(r'^design/(?P<pk>[0-9]+)/$', common.ProposalDetailView.as_view(), name='proposal-detail'),
    # Stimuli
    url(r"^stimuli/upload/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/(?P<group_id>[0-9]+)/(?P<slug>[A-Za-z0-9-_]+)/$",
        student.stimuli_upload, name='upload_stimuli'),
    url(r"^stimuli/requestapproval/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/(?P<group_id>[0-9]+)/(?P<slug>[A-Za-z0-9-_]+)/$",
        student.request_stimuli_approval, name='request_stimuli_approval'),
    url(r'^stimuli/preview/(?P<stimuli_id>[0-9]+)/$', student.preview_stimuli, name='preview_stimuli'),
    url(r'^stimuli/add_feedback/(?P<target_id>[0-9]+)/$', common.add_feedback, {'feedback_type': 'stimuli'}, name='stimuli_feedback'),
    url(r'^stimuli/(?P<pk>[0-9]+)/$', common.StimuliDetailView.as_view(), name='stimuli-detail'),
    # Experiment
    url(r"^experiment/(?P<year>[0-9]{4})/(?P<term>[A-Z]{2})/(?P<course_slug>[A-Za-z0-9-_]+)/(?P<group_id>[0-9]+)/(?P<slug>[A-Za-z0-9-_]+)/$",
        student.experiment_dash, name='experiment_dash'),
    url(r'^experiment/request_testing/(?P<experiment_id>[0-9]+)/$', student.request_experiment_approval, name='request_staff_testing'),
    url(r'^experiment/withdraw_testing/(?P<experiment_id>[0-9]+)/$', student.withdraw_from_testing, name='withdraw_from_testing'),
    url(r'^experiment/batches/(?P<experiment_id>[0-9]+)/$', common.experiment_batches, name='experiment_batches'),
    # Staff views from here on down
    url(r'^admin_dashboard/$', staff.admin_dashboard, name='admin_dashboard'),
    url(r'^manage_courses/$', staff.manage_courses, name='manage_courses'),
    url(r'^create_course/$', staff.create_course, name='create_course'),
    url(r'^course/(?P<course_id>[0-9]+)/add_students$', staff.add_students_course, name='add_students_course'),
    url(r'^course/(?P<course_id>[0-9]+)/remove_students$', staff.remove_students_course, name='remove_students_course'),
    url(r'^group/email/(?P<group_id>[0-9]+)/$', staff.email_group, name='email_group'),
    url(r'^group/manage/(?P<group_id>[0-9]+)/$', staff.manage_group, name='manage_group'),
    url(r'^group/add_members/(?P<group_id>[0-9]+)/$', staff.add_members_group, name='add_members_group'),
    url(r'^group/remove_members/(?P<group_id>[0-9]+)/$', staff.remove_members_group, name='remove_members_group'),
    url(r'^design/review/(?P<design_id>[0-9]+)/$', staff.review_design, name='review_design'),
    url(r'^design/approve/(?P<target_id>[0-9]+)/$', staff.toggle_approval, {'approval_type': 'design'}, name='approve_design'),
    url(r'^design/reject/(?P<target_id>[0-9]+)/$', staff.toggle_approval, {'approval_type': 'design'}, name='reject_design'),
    url(r'^design/request_revision/(?P<target_id>[0-9]+)/$', staff.request_revision, {'revision_type': 'design'}, name='revision_design'),
    url(r'^stimuli/review/(?P<stimuli_id>[0-9]+)/$', staff.review_stimuli, name='review_stimuli'),
    url(r'^stimuli/approve/(?P<target_id>[0-9]+)/$', staff.toggle_approval, {'approval_type': 'stimuli'}, name='approve_stimuli'),
    url(r'^stimuli/reject/(?P<target_id>[0-9]+)/$', staff.toggle_approval, {'approval_type': 'stimuli'}, name='reject_stimuli'),
    url(r'^stimuli/request_revision/(?P<target_id>[0-9]+)/$', staff.request_revision, {'revision_type': 'stimuli'}, name='revision_stimuli'),
    url(r'^experiment/review/(?P<experiment_id>[0-9]+)/$', staff.review_experiment, name='review_experiment'),
    url(r'^experiment/approve_running/(?P<experiment_id>[0-9]+)/$', staff.approve_experiment_running, name='approve_experiment_running'),
    url(r'^experiment/reject_running/(?P<experiment_id>[0-9]+)/$', staff.reject_experiment_running, name='reject_experiment_running'),
    url(r'^experiment/prerun_check/(?P<experiment_id>[0-9]+)/$', staff.preview_experiment_to_run, name='prerun_check'),
    url(r'^experiment/request_signoff/(?P<experiment_id>[0-9]+)/$', staff.request_final_signoff, name='request_final_signoff'),
    url(r'^experiment/run_mturk/(?P<experiment_id>[0-9]+)/$', staff.run_on_mturk, name='run_on_mturk'),
    url(r'^experiment/run_sandbox/(?P<experiment_id>[0-9]+)/$', staff.run_on_mturk, {'sandbox': True}, name='run_on_sandbox'),
]
