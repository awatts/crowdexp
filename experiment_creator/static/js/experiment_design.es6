// Based loosely on the Ractive TodoMVC example available at
// https://gist.github.com/c96db6acc974403b7c5b and
// http://examples.ractivejs.org/todos

/* eslint-env jquery es6 */
/* global _ */

let ractive = new Ractive({
  el: '#container',
  template: '#template',
  data: {
    manipulations: [],
    trialcount: 0,
    fillercount: 0,
    participants: 0,
    exname: '',
    alerts: []
  },
  createManipulation: function(event) {
    console.log('Creating a new manipulation');
    this.push('manipulations', {name: event.node.value, items: []});
    event.node.value = ''; // reset
  },
  createFactor: function(event) {
    console.log('Creating a factor on ' + event.keypath + '.items');
    this.push(event.keypath + '.items', event.node.value);
    event.node.value = ''; // reset
  },
  removeManipulation: function(event, index) {
    console.log('Removing manipulation at index ' + index);
    ractive.splice('manipulations', index, 1);
  },
  removeFactor: function(event, mindex, findex) {
    console.log('Removing factor from ' + 'manipulations.' + mindex + '.items at index ' + findex);
    ractive.splice('manipulations.' + mindex + '.items', findex, 1);
  },
  submitManipulationEdit: function(event, index) {
    this.set('manipulations[' + index + '].name', event.node.value);
    this.set('currentlyEditingManip', null);
  },
  setFactorEdit: function(findex, lindex) {
    this.set('currentlyEditingFactor', findex);
    this.set('currentlyEditingLevel', lindex);
  },
  submitFactorEdit: function(event, findex, lindex) {
    this.set(event.keypath, event.node.value);
    this.set('currentlyEditingFactor', null);
    this.set('currentlyEditingLevel', null);
  },
  checkCounts: function(event) {
    if (this.get('trialcount') > this.get('fillercount')) {
      this.push('alerts', {'message': 'It seems like you have more items than fillers. This is not recommended, since it can make it more likely that participants will implicitly or explicitly infer the purpose of your experiment. Consider adding more fillers.', 'type': 'warning'});
    }
  },
  inputKeypress: function(event) {
    if (event.original.keyCode == 13) {
      event.original.preventDefault();
      event.original.stopPropagation();
      //console.log('(Ractive) Enter pressed on ' + event.original.target);
      $(event.original.target).blur().focus();
      return false;
    }
  },
  decorators: {
      select: ( node ) => {
        setTimeout( () => {
          node.select();
        });

        return {
          // teardown is a noop
          teardown: () => {}
        };
      }
    }
});


function logArrayElements(element, index, array) {
  console.log('a[' + index + '] = ' + element.name + ' : ' + element.items);
}

ractive.observe('manipulations', function ( newVal, oldVal ) {
  newVal.forEach(logArrayElements);

  // Check that manipulation names are unique (error)
  let manip_names = _.pluck(ractive.get('manipulations'), 'name');
  if (typeof manip_names !== 'undefined') {
    let unique_names = _.uniq(manip_names);
    if (manip_names.length > unique_names.length) {
      ractive.push('alerts', {'message': 'Manipulation names must be unique.', type: 'error'});
    }
  }

  // Check that item names are unique across manipulations (warning)
  let item_names = _.flatten(_.pluck(ractive.get('manipulations'), 'items'));
  if (typeof item_names !== 'undefined') {
    let unique_items = _.uniq(item_names);
    if (item_names.length > unique_items.length) {
      ractive.push('alerts', {'message': 'Level names should be unique even across manipulations.', type: 'warning'});
    }
  }
});

ractive.observe('manipulations.*.items', function(newValue, oldValue, keypath) {
  console.log(keypath + ' changed from ' + oldValue + ' to ' + newValue);

  // Check that item names are unique within manipulations (error)
  let item_names = ractive.get(keypath);
  if (typeof item_names !== 'undefined') {
    if (item_names.length > _.uniq(item_names).length) {
      ractive.push('alerts', {'message': 'Level names must be unique within manipulations.', type: 'error'});
    }
  }
});

ractive.observe( 'exname', function ( newValue, oldValue, keypath ) {
  if (typeof oldValue !== 'undefined') {
    if (/\s+/.test(newValue)) {
      ractive.push('alerts', {'message': 'Experiment name should not contain any space or special characters.', 'type': 'error'});
      ractive.set(keypath, newValue.trim().replace(/\s+/, '.'));
    }
  }
}, {defer: true});


let jqxhr = $.getJSON( $('form').attr('action'))
  .done(function(data, textStatus, jqXHR) {
    try {
      ractive.set('trialcount', data.design.trialcount);
      ractive.set('fillercount', data.design.fillercount);
      ractive.set('exname', data.design.exname.trim().replace(/\s+/, '.'));
      ractive.set('manipulations', data.design.manipulations);
      ractive.set('participants', data.design.participants);
      CKEDITOR.instances.proposal.setData(data.proposal);
      console.log( "successfully loaded data" );
    } catch (e) {
      console.log('Error!: ' + e);
      console.log(data);
    }
  })
  .fail(function(jqXHR, textStatus, errorThrown) {
    console.log(jqXHR.status + ' ' +jqXHR.statusText + ": " + jqXHR.responseText);
    ractive.push('alerts', {'message': 'Error ' + jqXHR.status + ": " + jqXHR.responseText, 'type': 'error'});
  })
  .always(function() {
    console.log( "loading complete" );
  });

$('form').on('submit', (event) => {
  event.preventDefault();

  let form_valid = true;

  let ckcontent = CKEDITOR.instances.proposal.getData();

  let form_data = {manipulations: ractive.get('manipulations'),
    participants: ractive.get('participants'),
    trialcount: ractive.get('trialcount'),
    fillercount: ractive.get('fillercount'),
    exname: ractive.get('exname')
  };

  if (!form_data.participants) {
    ractive.push('alerts', {'message': '"Number of participants" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.trialcount) {
    ractive.push('alerts', {'message': '"Number of critical items" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.fillercount) {
    ractive.push('alerts', {'message': '"Number of filler items" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (!form_data.exname) {
    ractive.push('alerts', {'message': '"Experiment Name" must have a value!', 'type': 'error'});
    form_valid = false;
  }

  if (form_data.manipulations.length == 0) {
    ractive.push('alerts', {'message': 'You must have at least one manipulation', 'type': 'error'});
    form_valid = false;
  } else {
    if (_.max(_.map(form_data.manipulations, x => x.items.length)) == 1) {
      ractive.push('alerts', {'message': 'At least one manipulation must have two or more levels', 'type': 'error'});
      form_valid = false;
    }
    // TODO: finish writing this validation
  }

  if(form_valid) {
    $.ajax({
      url: $('form').attr('action'),
      data: {
        proposal: ckcontent,
        design: JSON.stringify(form_data)
      },
      method: "POST",
      beforeSend: (xhr, settings) => {
        xhr.setRequestHeader("X-CSRFToken", $('[name="csrfmiddlewaretoken"]').val());
      },
    }).done((data, textStatus, jqXHR) => {
      console.log('Data uploaded: ' + jqXHR.status + ": " + jqXHR.responseText);
      $('[type="submit"]').hide();
      $('a[rel="prev"]').removeClass('hidden');
      $('a.button.success').removeClass('disabled');
      ractive.push('alerts', {'message': 'Design saved', 'type': 'success'});
    }).fail((jqXHR, textStatus, errorThrown) => {
      console.log(jqXHR.status + ' ' + jqXHR.statusText + ": " + jqXHR.responseText);
      ractive.push('alerts', {'message': 'Error ' + jqXHR.status + ": " + jqXHR.responseText, 'type': 'error'});
    }).always(() => {
    });
  } else {
    return false;
  }
});
