from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import hashlib
from datetime import date
from os.path import splitext
from random import randint

import pandas as pd
from allauth.account.models import EmailAddress
from allauth.socialaccount.models import SocialAccount
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.contrib.postgres.fields.jsonb import JSONField
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
from filer.fields.folder import FilerFolderField
from filer.models.filemodels import File
from polymorphic.models import PolymorphicModel

from .util.tagged_spr_parser import parse_tagged_sentence
from .util.validate_design import validate_design
from .util.validate_excel_spr import validate_excel_spr


def _six_digit_pin():
    return ''.join([str(randint(0, 9)) for x in range(6)])


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')

    def __str__(self):
        return "{}'s profile".format(self.user.username)

    class Meta:
        db_table = 'user_profile'

    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def get_display_name(self):
        if self.user.first_name and self.user.last_name:
            return self.user.get_full_name()
        else:
            return self.user.username

    def get_list_name(self):
        if self.user.first_name and self.user.last_name:
            list_name = '%s, %s' % (self.user.last_name, self.user.first_name)
            return list_name.strip()
        else:
            return self.user.username

    def profile_image_url(self, size=75):
        google_uid = SocialAccount.objects.filter(user_id=self.user.id, provider='google')

        if len(google_uid):
            return google_uid[0].get_avatar_url()

        return "https://www.gravatar.com/avatar/{}?s={}&d=identicon".format(hashlib.md5(self.user.email.encode('utf-8')).hexdigest(), size)

    def profile_url(self):
        return reverse('user_profile', kwargs={'user_id': self.user.id})

    def can_run_experiments(self):
        return self.user.has_perm('experiment_creator.can_run_experiments')


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class Course(models.Model):

    """Course experiments will be run for."""

    TERM_OPTIONS = (
        ('AU', 'Autumn'),
        ('SP', 'Spring'),
        ('SU', 'Summer'),
    )
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    year = models.PositiveSmallIntegerField(
        default=date.today().year,
        validators=[
            RegexValidator(r'^2[0-9]{3}$',
                           ('Enter a valid year. '
                            'Year must be 4 digits, 2xxx'), 'invalid'),
        ],
        help_text="Please enter a 4 digit year, 2xxx"
    )
    term = models.CharField(max_length=2, choices=TERM_OPTIONS)
    current = models.BooleanField(default=True)
    students = models.ManyToManyField(User, related_name='enrolled_courses', limit_choices_to={'is_staff': False}, blank=True)
    instructors = models.ManyToManyField(User, related_name='managed_courses', limit_choices_to={'is_staff': True}, blank=True)

    def __str__(self):
        return '%s %d - %s' % (self.term, self.year, self.name)

    def get_absolute_url(self):
        return reverse('course_dash', kwargs={
            'year': self.year,
            'term': self.term,
            'slug': self.slug,
        })


class CourseGroup(models.Model):
    """Group within a Course."""

    name = models.CharField(max_length=128)
    course = models.ForeignKey(Course)
    joining_pin = models.CharField(max_length=6,
                                   default=_six_digit_pin,
                                   validators=[
                                        RegexValidator(r'^[0-9]{6}$',
                                                       ('Enter a valid PIN. '
                                                        'This value must contain exactly 6 digits'), 'invalid'),
                                    ],
                                   help_text="Please enter a 6 digit PIN"
    )
    # XXX: ideally would limit_choices_to students in that course, something like
    # limit_choices_to=lambda: {'enrolled_courses': course}
    # but there's not an apparent reasonable to implement way to do it (can't refer to course)
    # need to filter in forms
    students = models.ManyToManyField(User,
                                      related_name='course_groups',
                                      blank=True)
    # slug = models.SlugField(max_length=128, unique=True)

    class Meta:
        unique_together = ('name', 'course')

    def __str__(self):
        return 'Group #%d: %s' % (self.id, self.name)

    def get_members(self, **kwargs):
        return self.students.filter(**kwargs)

    def get_absolute_url(self):
        return reverse('group_dash', kwargs={
            'year': self.course.year,
            'term': self.course.term,
            'course_slug': self.course.slug,
            'group_id': self.id,
        })

    def new_random_pin(self):
        """Generate a new group PIN"""
        self.joining_pin = _six_digit_pin()
        self.save()


class Project(models.Model):

    """Project within a Course."""

    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    group = models.ForeignKey(CourseGroup)
    active = models.BooleanField(default=True)

    def __str__(self):
        return 'Project #%d: %s' % (self.id, self.name)

    def get_absolute_url(self):
        return reverse('project_dash', kwargs={'project_id': self.id})


class Experiment(models.Model):

    """The actual experiment."""

    STATUS_OPTIONS = (
        ('DES', 'Proposal and Design'),
        ('STM', 'Stimuli'),
        ('GTS', 'Group Testing'),
        ('STS', 'Staff Testing'),
        ('RUN', 'Collecting Data'),
        ('ANL', 'Analyzing Data'),
    )

    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='DES')
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128)
    group = models.ForeignKey(CourseGroup)
    project = models.ForeignKey(Project)
    folder = FilerFolderField(null=True, blank=True, related_name="experiment_folder")

    def __str__(self):
        return 'Experiment #%d: %s' % (self.id, self.name)

    def get_absolute_url(self):
        return reverse('experiment_dash', kwargs={
            'year': self.group.course.year,
            'term': self.group.course.term,
            'course_slug': self.group.course.slug,
            'group_id': self.group.id,
            'slug': self.slug
        })

    class Meta:
        permissions = (('can_run_experiments', 'Can run experiments on MTurk'),)


class ExperimentDocument(PolymorphicModel):

    """Abstract base class for Experiment related documents."""

    group = models.OneToOneField(CourseGroup)
    experiment = models.OneToOneField(Experiment)

    class Meta:
        abstract = True


class StimuliDesign(ExperimentDocument):

    """Experiment proposal and design of Self Paced Reading experiment."""

    STATUS_OPTIONS = (
        ('NEW', 'Not yet specified'),
        ('REQ', 'Approval Pending'),
        ('APP', 'Approved'),
        ('REV', 'Revision Requested by Instructor'),
        ('RUP', 'Revised - Approval not yet requested'),
    )
    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='NEW')
    proposal = RichTextField(default='<p>Describe your proposed experiment here.</p>')
    design = JSONField(default={'manipulations': [], 'trialcount': 0, 'fillercount': 0,
                                'participants': 0, 'duration': 30, 'exname': ''})
    errors = JSONField(default={})

    def __str__(self):
        return 'Design Proposal #%s' % self.id

    def get_absolute_url(self):
        return reverse('proposal-detail', kwargs={'pk': self.id})

    def is_approved(self):
        return self.status == 'APP'

    def get_level_count(self):
        if len(self.design['manipulations']) == 0:
            return "0"
        else:
            return "x".join([str(len(x['items'])) for x in self.design['manipulations']])

    def can_download_template(self):
        """
        Only allow download of template if certain criteria are met.
          * Proposal isn't default
          * At least 1 factor with 2 levels
        """
        has_proposal = self.proposal != self.proposal.default
        has_manipulations = self.get_level_count() not in ('0', '1x1')

        return has_proposal and has_manipulations

    def validate(self):
        try:
            self.errors = validate_design(self.design)
            self.save()
        except Exception as err:
            self.errors = {'errors': [{'Exception occurred during validation': str(err)}]}


class ExperimentStimuli(ExperimentDocument):

    """Stimuli for Self Paced Reading experiment in Excel XLSX format."""

    STATUS_OPTIONS = (
        ('NEW', 'Not yet specified'),
        ('UPL', 'Approval not yet requested'),
        ('REQ', 'Approval Pending'),
        ('APP', 'Approved'),
        ('REV', 'Revision Requested by Instructor'),
        ('RUP', 'Revised - Approval not yet requested'),
    )

    status = models.CharField(max_length=3, choices=STATUS_OPTIONS, default='NEW')
    folder = FilerFolderField(null=True, blank=True, related_name="stimuli_folder")
    errors = JSONField(default={})

    def __str__(self):
        return 'ExperimentStimuli #%s' % self.id

    def get_absolute_url(self):
        return reverse('stimuli-detail', kwargs={'pk': self.id})

    def validate(self):
        """Run stimuli validator on the current uploaded file."""
        self.errors = {}
        self.save()
        try:
            if self.folder and self.folder.file_count > 0:
                design = self.experiment.stimulidesign.design
                manips = [m['items'] for m in design['manipulations']]
                last_stim = self.folder.files.last().file
                self.errors = validate_excel_spr(last_stim,
                                                 design['exname'],
                                                 int(design['trialcount']),
                                                 int(design['fillercount']),
                                                 manips)
                self.save()
        except Exception as e:  # FIXME: what kind of exceptions might happen? catch those and handle specifically
            self.errors = {"errors": [{"category": "validation", "message": "Fatal Validation Error"}], "warnings": {}}
            self.save()

    def generate_parsed(self):
        """
        Parse all the 'Sentences' in the stimulus file.

        Returns a list of dictionaries with of each row from the stimulus file
        with sentence replaced with the output of parse_tagged_sentence
        """
        if self.folder and self.folder.file_count > 0:
            last_stim = self.folder.files.last().file

            df = pd.read_excel(last_stim)
            # Drop all columns past where 'Answer' should be
            df.drop(df.columns[6:], inplace=True, axis=1)
            # XXX: Could I rename columns in pandas and use df['Sentence'].apply(parse_tagged_sentence) to do same thing faster?
            # df['sentence'] = df['Sentence'].apply(parse_tagged_sentence)
            # df.rename(columns={'Experiment': 'experiment', 'ItemName': 'item', 'Condition': 'condition',
            #                    'Question': 'question', 'Answer': 'answer'}, inplace=True)
            # df.drop('Sentence', axis=1, inplace=True)
            # just need to add the 'row' column
            # return df.to_dict(orient='records')
            return [{'row': row[0] + 2,
                     'experiment': row[1],
                     'item': row[2],
                     'condition': row[3],
                     'sentence': parse_tagged_sentence(row[4]),
                     'question': row[5],
                     'answer': row[6]
                     } for row in df.itertuples()]
        else:
            return []

    class Meta:
        verbose_name_plural = "stimuli"


class WordDocument(File):

    """Subclass of Django Filer's File model for MS Word documents."""

    file_type = 'MS-Word'
    _icon = "word"

    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.doc', '.docx', ]
        ext = splitext(iname)[1].lower()
        return ext in filename_extensions


class ExcelDocument(File):

    """Subclass of Django Filer's File model for MS Excel documents."""

    file_type = 'MS-Excel'
    _icon = "excel"

    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        # the extensions we'll recognise for this file type
        filename_extensions = ['.xls', '.xlsx', ]
        ext = splitext(iname)[1].lower()
        return ext in filename_extensions


class MTurkHIT(models.Model):
    """A HIT object returned by CreateHIT"""
    experiment = models.ForeignKey(Experiment)
    props = JSONField(default={})
    sandbox = models.BooleanField(default=False)
