from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.text import slugify
from django.views.generic import DetailView
from filer.models import Folder

from ..forms import CourseGroupForm
from ..models import Course, CourseGroup, Project, Experiment, StimuliDesign, ExperimentStimuli, MTurkHIT
from ..util.mturk import update_hit_info


def university_email_or_staff_check(user):
    """
    Check user's email to verify they are a student or staff.
    """
    # XXX: if a user fails it goes into a horrible redirect loop. How to prevent that?
    # ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = False
    if user.is_anonymous():
        return False
    if user.is_authenticated():
        return user.email.endswith(settings.ALLOWED_ACCOUNT_DOMAINS) or user.is_staff

    return False


@login_required
def user_profile(request, user_id):

    user = get_object_or_404(User, id=user_id)
    return render(request, 'experiment_creator/user_profile.html', {'user': user})


@user_passes_test(university_email_or_staff_check)
def add_feedback(request, feedback_type, target_id):
    """Add top level feedback (comment) to a design or stimuli object"""
    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[feedback_type]

    target = get_object_or_404(target_type, id=target_id)

    return render(request, 'experiment_creator/add_feedback.html', {
        'feedback_target': target,
        'feedback_type': feedback_type,
        'next': request.META.get('HTTP_REFERER', reverse('comments-comment-done'))
    })


@user_passes_test(university_email_or_staff_check)
def create_group(request, year, term, course_slug):
    """
    Create a CourseGroup for a Course.

    Temporarily staff only, but eventually want students to create their own
    """
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = CourseGroupForm(request.POST, course_id=course.id)
        if form.is_valid():
            group_name = form.cleaned_data['name']
            new_group = CourseGroup(name=group_name, course=course)
            new_group.save()

            group_students = form.cleaned_data['students']
            new_group.students.set(group_students)
            # If a student creates a group, add them to the group even if they didn't add themselves
            if not request.user.is_staff:
                if request.user not in new_group.students.all():
                    new_group.students.add(request.user)
                    messages.add_message(request, messages.INFO, 'Added you to the new group you created')

            # Create related objects that have to exist to be able to do anything

            # Project
            project_name = '{} Project #1'.format(group_name)
            new_project = Project(name=project_name, slug=slugify(project_name), group=new_group)
            new_project.save()

            # Folder
            term_folder, _ = Folder.objects.get_or_create(name="{}{}".format(course.term, course.year))
            course_folder, _ = term_folder.children.get_or_create(name=course.name)
            project_folder, _ = course_folder.children.get_or_create(name=slugify(group_name))

            # Experiment
            experiment_name = '{} Experiment #1'.format(group_name)
            new_experiment = Experiment(name=experiment_name,
                                        slug=slugify(experiment_name),
                                        group=new_group,
                                        project=new_project,
                                        folder=project_folder)
            new_experiment.save()
            messages.add_message(request, messages.SUCCESS, 'Created {}'.format(new_group.name))
            return HttpResponseRedirect(reverse('course_dash',
                                                kwargs={'year': course.year,
                                                        'term': course.term,
                                                        'slug': course.slug}))
    else:
        form = CourseGroupForm(course_id=course.id)

    return render(request, 'experiment_creator/create_group.html', {
        'course': course,
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def experiment_batches(request, experiment_id):
    """Show MTurk HIT batches for a given experiment"""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    batches = MTurkHIT.objects.filter(experiment=experiment)

    mturk_hits = MTurkHIT.objects.filter(experiment=experiment, sandbox=False)
    sandbox_hits = MTurkHIT.objects.filter(experiment=experiment, sandbox=True)

    update_hit_info(mturk_hits)
    update_hit_info(sandbox_hits, sandbox=True)

    return render(request, 'experiment_creator/experiment_batches.html', {
        'experiment': experiment,
        'batches': batches,
    })


class ProposalDetailView(DetailView, UserPassesTestMixin):
    # XXX: maybe move DetailView views into own file?
    queryset = StimuliDesign.objects.all()

    def test_func(self):
        return university_email_or_staff_check(self.request.user)

    def get_object(self):
        return super(ProposalDetailView, self).get_object()


class StimuliDetailView(DetailView, UserPassesTestMixin):
    queryset = ExperimentStimuli.objects.all()

    def test_func(self):
        return university_email_or_staff_check(self.request.user)

    def get_object(self):
        return super(StimuliDetailView, self).get_object()
