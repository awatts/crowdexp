from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import logging
from functools import reduce
from math import ceil
from operator import mul

from boto.mturk.connection import MTurkConnection, MTurkRequestError, Price
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils.text import slugify
from filer.models import Folder

from experiment_runner.models import Experiment as RunnerExperiment
from ..forms import CourseForm, EmailGroupForm, GroupMemberForm, GroupNonMemberForm
from ..forms import CourseStudentsForm, CourseNonStudentsForm
from ..models import Course, CourseGroup, StimuliDesign, ExperimentStimuli, Experiment
from ..util.contact import email_course_group, email_mturk_runners
from ..util.mturk import run_mturk_spr_experiment

logger = logging.getLogger(__name__)

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

AWS_ACCESS_KEY = AWS_SETTINGS['AWS_ACCESS_KEY']
AWS_SECRET_KEY = AWS_SETTINGS['AWS_SECRET_KEY']


@staff_member_required
def admin_dashboard(request):
    """
    Admin dashboard
    """
    managed_courses = request.user.managed_courses.all()
    managed_groups = CourseGroup.objects.filter(course__in=managed_courses)

    designs = StimuliDesign.objects.filter(group__in=managed_groups)
    stimuli = ExperimentStimuli.objects.filter(group__in=managed_groups)
    experiments = Experiment.objects.filter(group__in=managed_groups)

    return render(request, 'experiment_creator/admin_dashboard.html', {
        'pending_designs': designs.filter(status='REQ'),
        'rejected_designs': designs.filter(status='REV'),
        'approved_designs': designs.filter(status='APP'),
        'revised_designs': designs.filter(status='RUP'),

        'new_stimuli': stimuli.filter(status__in=('UPL', 'RUP')),
        'pending_stimuli': stimuli.filter(status='REQ'),
        'rejected_stimuli': stimuli.filter(status='REV'),
        'approved_stimuli': stimuli.filter(status='APP'),

        'staff_testing': experiments.filter(status='STS'),
        'running_experiments': experiments.filter(status='RUN'),
        'analysis_experiments': experiments.filter(status='ANL'),

        'courses': managed_courses,
    })


@staff_member_required
def manage_courses(request):
    """Staff view to manage courses"""
    current_courses = Course.objects.filter(current=True)
    past_courses = Course.objects.filter(current=False)

    return render(request, 'experiment_creator/manage_courses.html', {
        'current_courses': current_courses,
        'past_courses': past_courses
    })


@staff_member_required
def create_course(request):
    """Create a new course"""
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = CourseForm(request.POST)
        if form.is_valid():
            course_year = form.cleaned_data['year']
            course_term = form.cleaned_data['term']
            course_name = form.cleaned_data['name']
            course_current = form.cleaned_data['current']
            course_students = form.cleaned_data['students']
            course_instructors = form.cleaned_data['instructors']
            course_slug = slugify('{} {} {}'.format(course_name, course_term, course_year))

            new_course = Course(name=course_name, year=course_year, term=course_term,
                                current=course_current, slug=course_slug)
            new_course.save()

            new_course.students.set(course_students)
            new_course.instructors.set(course_instructors)
            new_course.save()

            messages.add_message(request, messages.SUCCESS, 'Created {} ({} {})'.format(new_course.name,
                                                                                        new_course.term,
                                                                                        new_course.year))

            term_folder, term_created = Folder.objects.get_or_create(name="{}{}".format(new_course.term, new_course.year))
            if term_created:
                messages.add_message(request, messages.SUCCESS,
                                     'Created folder for term: {} {} at {}'.format(new_course.term,
                                                                                   new_course.year,
                                                                                   term_folder.pretty_logical_path))

            course_folder, course_folder_created = term_folder.children.get_or_create(name=new_course.name)
            if course_folder_created:
                messages.add_message(request, messages.SUCCESS,
                                     'Created folder for course: {} at {}'.format(new_course.name,
                                                                                  term_folder.pretty_logical_path))

            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseForm()

    return render(request, 'experiment_creator/create_course.html', {
        'form': form,
    })


@staff_member_required
def review_design(request, design_id):
    """Staff view to review experiment design."""
    design = get_object_or_404(StimuliDesign, id=design_id)

    return render(request, 'experiment_creator/design_review.html', {
        'design': design,
    })


@staff_member_required
def review_stimuli(request, stimuli_id):
    """Staff view to review experiment stimuli."""
    stimuli = get_object_or_404(ExperimentStimuli, id=stimuli_id)

    if not stimuli.folder:
        stimuli.folder, _ = stimuli.experiment.folder.children.get_or_create(name='Stimuli')

    return render(request, 'experiment_creator/stimuli_review.html', {
        'stimuli': stimuli,
        'stimuli_folder': stimuli.folder
    })


@staff_member_required
def review_experiment(request, experiment_id):
    """Staff view to review experiment before running."""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    runner_experiment = get_object_or_404(RunnerExperiment, name=experiment.slug)

    return render(request, 'experiment_creator/experiment_review.html', {
        'experiment': experiment,
        'runner_experiment': runner_experiment,
    })


@staff_member_required
def approve_experiment_running(request, experiment_id):
    """Staff view to approve an experiment to run"""
    experiment = get_object_or_404(Experiment, id=experiment_id)

    experiment.status = 'RUN'
    experiment.save()

    subject = "Your experiment is ready to run"
    body = "{} has approved your experiment to run. Data collection will begin soon.".format(request.user.get_full_name())
    email_course_group(experiment.group.id, subject, body)

    messages.add_message(request, messages.INFO, 'Approved "{}" for running'.format(experiment.name))

    return HttpResponseRedirect(reverse('prerun_check', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def reject_experiment_running(request, experiment_id):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    # Reset experiment status to editing stimuli
    experiment.status = 'STM'
    experiment.save()

    # Reset stimuli status to revision requested
    stimuli = experiment.experimentstimuli
    stimuli.status = 'REV'
    stimuli.save()

    subject = "Problems with experiment. Stimuli sent back for revision"
    body = "{} found errors with your experiment. Please revise your stimuli.".format(request.user.get_full_name())
    email_course_group(experiment.group.id, subject, body)

    messages.add_message(request, messages.WARNING, 'Sent "{}" back for stimuli revision'.format(experiment.name))

    return HttpResponseRedirect(reverse('review_experiment', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def preview_experiment_to_run(request, experiment_id):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    design = experiment.stimulidesign.design

    participants = int(design.get('participants', 0))
    manipulations = design.get('manipulations', [{'items': []}])
    duration = int(design.get('duration', 30))

    total_items = int(design.get('trialcount', 0)) + int(design.get('fillercount', 0))

    list_count = reduce(mul, [len(x['items']) for x in manipulations]) * 2

    estimated_duration = ceil(total_items / 2)

    hit_price = duration / 60 * 6  # $6/hr

    amazon_cut = 0.40 if participants >= 10 else 0.20

    base_hit_cost = participants * hit_price

    total_hit_cost = base_hit_cost * (1 + amazon_cut)

    try:
        mtc = MTurkConnection(aws_access_key_id=AWS_ACCESS_KEY, aws_secret_access_key=AWS_SECRET_KEY)
        balance = mtc.get_account_balance()[0]
    except MTurkRequestError:
        balance = Price(0)

    return render(request, 'experiment_creator/experiment_prerun.html', {
        'experiment': experiment,
        'participants': participants,
        'list_count': list_count,
        'total_items': total_items,
        'participants_per_list': participants / list_count,
        'account_balance': balance.amount,
        'duration': duration,
        'estimated_duration': estimated_duration,
        'hit_price': hit_price,
        'amazon_cut': amazon_cut * 100,
        'base_hit_cost': base_hit_cost,
        'total_hit_cost': total_hit_cost,
        'sufficient_funds': balance.amount > total_hit_cost
    })


@staff_member_required
def request_final_signoff(request, experiment_id):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    messages.add_message(request, messages.INFO, 'Requested final sign off to run experiment')

    subject = '{} requests to run {} on MTurk'.format(request.user.get_full_name(), experiment.group.name)
    body = 'You can view it for final approval at {}'.format(reverse('run_on_mturk', kwargs={'experiment_id': experiment.id}))

    error_string = email_mturk_runners(subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, 'Emailed MTurk runners')

    return HttpResponseRedirect(reverse('prerun_check', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def run_on_mturk(request, experiment_id: int, sandbox: bool=False):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    experiment_url = request.build_absolute_uri(reverse('spr_experiment', kwargs={'experiment': experiment.slug}))

    num_hits = run_mturk_spr_experiment(experiment, experiment_url, sandbox)

    if num_hits == 0:
        messages.add_message(request, messages.ERROR, 'Failed to create HITs. See server log.')
    else:
        messages.add_message(request, messages.INFO, 'Created {} HITs'.format(num_hits))

    return HttpResponseRedirect(reverse('experiment_batches', kwargs={'experiment_id': experiment.id}))


@staff_member_required
def toggle_approval(request, approval_type, target_id):
    """Staff view to approve or reject experiment materials."""
    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[approval_type]

    target = get_object_or_404(target_type, id=target_id)

    if target.status == 'APP':
        target.status = 'REV'
    else:
        target.status = 'APP'
    target.save()

    messages.add_message(request, messages.INFO, '{} status set to {}.'.format(target, target.get_status_display()))

    subject = "{} set {}'s {} to {}".format(request.user.get_full_name(), target.group.name, approval_type, target.get_status_display())
    body = 'You can view the {} at {}'.format(approval_type, request.build_absolute_uri(target.get_absolute_url()))

    error_string = email_course_group(target.group.id, subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, 'Emailed {}'.format(target.group.name))

    return HttpResponseRedirect(reverse('review_{}'.format(approval_type),
                                        kwargs={'{}_id'.format(approval_type): target.id}))


@staff_member_required
def request_revision(request, revision_type, target_id):
    """Request revision of experiment materials."""

    target_type = {
        'design': StimuliDesign,
        'stimuli': ExperimentStimuli,
    }[revision_type]

    target = get_object_or_404(target_type, id=target_id)
    target.status = 'REV'
    target.save()

    messages.add_message(request, messages.INFO, 'Requested revision of {}'.format(revision_type))

    subject = '{} requested {} revisions from {}'.format(request.user.get_full_name(), revision_type, target.group.name)
    body = 'You can view the {} at {}'.format(revision_type, request.build_absolute_uri(target.get_absolute_url()))

    error_string = email_course_group(target.group.id, subject, body)

    if error_string:
        messages.add_message(request, messages.ERROR, error_string)
    else:
        messages.add_message(request, messages.INFO, 'Emailed {}'.format(target.group.name))

    return HttpResponseRedirect(reverse('review_{}'.format(revision_type),
                                        kwargs={'{}_id'.format(revision_type): target.id}))


@staff_member_required
def email_group(request, group_id):
    """Email all members of a group."""
    group = get_object_or_404(CourseGroup, id=group_id)
    if request.method == 'POST':
        # Create a form instance from POST data.
        form = EmailGroupForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            body = form.cleaned_data['body']
            error_string = email_course_group(group_id, subject, body)

            if error_string:
                messages.add_message(request, messages.ERROR, error_string)
            else:
                messages.add_message(request, messages.INFO, 'Emailed {}'.format(group.name))

            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = EmailGroupForm()

    return render(request, 'experiment_creator/email_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def manage_group(request, group_id):
    """Manage a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    return render(request, 'experiment_creator/manage_group.html', {
        'group': group
    })


@staff_member_required
def add_members_group(request, group_id):
    """Add members to a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    if request.method == 'POST':
        form = GroupNonMemberForm(request.POST, group_id=group_id)
        if form.is_valid():
            additions = form.cleaned_data['non_members']
            group.students.add(*additions)
            messages.add_message(request, messages.INFO, 'Added {} students to {}'.format(len(additions), group.name))
            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = GroupNonMemberForm(group_id=group_id)

    return render(request, 'experiment_creator/add_members_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def remove_members_group(request, group_id):
    """Remove members from a group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    if request.method == 'POST':
        form = GroupMemberForm(request.POST, group_id=group_id)
        if form.is_valid():
            removals = form.cleaned_data['members']
            group.students.remove(*removals)
            messages.add_message(request, messages.INFO, 'Removed {} students from {}'.format(len(removals), group.name))
            return HttpResponseRedirect(reverse('manage_group', kwargs={'group_id': group.id}))
    else:
        form = GroupMemberForm(group_id=group_id)

    return render(request, 'experiment_creator/remove_members_group.html', {
        'form': form,
        'group': group
    })


@staff_member_required
def add_students_course(request, course_id):
    """Add students to a course."""
    course = get_object_or_404(Course, id=course_id)

    if request.method == 'POST':
        form = CourseNonStudentsForm(request.POST, course_id=course_id)
        if form.is_valid():
            additions = form.cleaned_data['non_members']
            course.students.add(*additions)
            messages.add_message(request, messages.INFO, 'Added {} students to {}'.format(len(additions), course.name))
            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseNonStudentsForm(course_id=course_id)

    return render(request, 'experiment_creator/add_students_course.html', {
        'course': course,
        'form': form
    })


@staff_member_required
def remove_students_course(request, course_id):
    """Remove students from a course."""
    course = get_object_or_404(Course, id=course_id)

    if request.method == 'POST':
        form = CourseStudentsForm(request.POST, course_id=course_id)
        if form.is_valid():
            removals = form.cleaned_data['members']
            course.students.remove(*removals)
            messages.add_message(request, messages.INFO, 'Removed {} students from {}'.format(len(removals), course.name))
            return HttpResponseRedirect(reverse('manage_courses'))
    else:
        form = CourseStudentsForm(course_id=course_id)

    return render(request, 'experiment_creator/remove_students_course.html', {
        'course': course,
        'form': form
    })
