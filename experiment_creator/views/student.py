from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import json
import os.path

from actstream import action
from actstream.models import any_stream
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseForbidden, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from openpyxl.writer.excel import save_virtual_workbook

from experiment_runner.models import Experiment as RunnerExperiment
from .common import university_email_or_staff_check
from ..forms import ExcelForm, CourseJoinForm, CourseGroupJoinForm, CourseGroupExitForm
from ..models import Course, CourseGroup
from ..models import Experiment, Project
from ..models import StimuliDesign, ExperimentStimuli
from ..util.contact import email_course_staff
from ..util.create_template import create_xlsx_template


# https://docs.djangoproject.com/en/1.8/topics/auth/default/#limiting-access-to-logged-in-users-that-pass-a-test


@login_required
def landing(request):
    """Landing page"""
    user_courses = request.user.enrolled_courses.filter(current=True)
    managed_courses = request.user.managed_courses.filter(current=True)
    user_groups = [c for c in request.user.course_groups.all() if c.course.current]
    courses = Course.objects.filter(current=True)
    return render(request, 'experiment_creator/user_landing.html', {
        'available_courses': set(courses) - set(user_courses),
        'user_courses': user_courses,
        'user_groups': user_groups,
        'managed_courses': managed_courses
    })


@login_required
def dashboard(request):
    """General user dashboard."""
    # user_groups = [c for c in CourseGroup.objects.filter(user=request.user) if c.course.current]

    # Since this is the default root url, to avoid infinite redirect loop, just
    # require login and do the test and a proper redirect inside the function
    if not university_email_or_staff_check(request.user):
        return HttpResponseRedirect(reverse('landing'))

    user_groups = [c for c in request.user.course_groups.all() if c.course.current]
    if not user_groups:   # User doesn't have any groups yet
        return redirect('landing')

    return render(request, 'experiment_creator/dashboard.html', {
        'group_stream': {g.name: any_stream(g) for g in user_groups},
    })


@user_passes_test(university_email_or_staff_check)
def course_dash(request, year, term, slug):
    """Course level dashboard."""
    course = get_object_or_404(Course, year=year, term=term, slug=slug)

    return render(request, 'experiment_creator/course.html', {'course': course, })


@user_passes_test(university_email_or_staff_check)
def group_dash(request, year, term, course_slug, group_id):
    """Group level dashboard."""
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    ex_group = get_object_or_404(CourseGroup, course=course, id=group_id)
    return render(request, 'experiment_creator/group.html', {
        'course': course,
        'group': ex_group,
    })


@user_passes_test(university_email_or_staff_check)
def join_course(request, course_id):
    """
    Join a single course
    """

    course = get_object_or_404(Course, id=course_id)
    request.user.enrolled_courses.add(course)
    messages.add_message(request, messages.SUCCESS, 'Joined {} ({} {})'.format(course.name, course.term, course.year))

    referer = request.META.get('HTTP_REFERER', reverse('landing'))
    return HttpResponseRedirect(referer)


@user_passes_test(university_email_or_staff_check)
def manage_course_membership(request):
    """
    Change which courses user is a member of
    """
    if request.method == 'POST':
        data = request.POST
        form = CourseJoinForm(data)
        if form.is_valid():
            if form.has_changed():
                request.user.enrolled_courses.set(form.cleaned_data['courses'])
                messages.add_message(request, messages.SUCCESS, 'Courses updated')
            else:
                messages.add_message(request, messages.INFO, 'No course changes were submitted')
        else:
            messages.add_message(request, messages.ERROR, 'Failed to join courses')
    else:
        form = CourseJoinForm(initial={'courses': [c.id for c in request.user.enrolled_courses.all()]})

    return render(request, 'experiment_creator/manage_course_membership.html', {
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def join_group(request, group_id):
    """
    Join a course group
    """

    group = get_object_or_404(CourseGroup, id=group_id)
    request.user.course_groups.add(group)
    messages.add_message(request, messages.SUCCESS, 'Joined {}'.format(group.name))

    # XXX: probably should redirect to group page
    referer = request.META.get('HTTP_REFERER', reverse('landing'))
    return HttpResponseRedirect(referer)


@user_passes_test(university_email_or_staff_check)
def leave_group(request, group_id):
    """
    Leave a course group
    """
    group = get_object_or_404(CourseGroup, id=group_id)
    request.user.course_groups.remove(group)
    messages.add_message(request, messages.SUCCESS, 'Left group: {}'.format(group.name))

    referer = request.META.get('HTTP_REFERER', reverse('landing'))
    return HttpResponseRedirect(referer)


@user_passes_test(university_email_or_staff_check)
def request_group_membership(request, group_id):
    """Request membership to a Course Group."""
    group = get_object_or_404(CourseGroup, id=group_id)
    if request.method == 'POST':
        data = request.POST
        data['group'] = group
        form = CourseGroupJoinForm(data)
        if form.is_valid():
            joining_pin = form.cleaned_data['joining_pin']
            if joining_pin == group.joining_pin:
                request.user.course_groups.add(group)
                messages.add_message(request, messages.SUCCESS, 'Joined {}'.format(group.name))
                return HttpResponseRedirect(
                    reverse('course_dash',
                            kwargs={'year': group.course.year,
                                    'term': group.course.term,
                                    'slug': group.course.slug}))
        else:
            messages.add_message(request, messages.ERROR, 'Failed to join {}'.format(group.name))
    else:
        form = CourseGroupJoinForm()

    return render(request, 'experiment_creator/join_group.html', {
        'group': group,
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def request_group_leaving(request, group_id):
    """Request to leave a Course Group."""
    group = get_object_or_404(CourseGroup, id=group_id)

    if group not in request.user.course_groups.all():
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    if request.method == 'POST':
        data = request.POST
        data['group'] = group
        form = CourseGroupExitForm(data)
        if form.is_valid():
            joining_pin = form.cleaned_data['joining_pin']
            if joining_pin == group.joining_pin:
                request.user.course_groups.remove(group)
                messages.add_message(request, messages.SUCCESS, 'Left {}'.format(group.name))
                return HttpResponseRedirect(
                    reverse('course_dash',
                            kwargs={'year': group.course.year,
                                    'term': group.course.term,
                                    'slug': group.course.slug}))
        else:
            messages.add_message(request, messages.ERROR, 'Failed to leave {}'.format(group.name))
    else:
        form = CourseGroupExitForm()

    return render(request, 'experiment_creator/leave_group.html', {
        'group': group,
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def project_dash(request, project_id):
    """
    Project level dashboard.

    Largely unimplemented
    """
    project = get_object_or_404(Project, id=project_id)

    return render(request, 'experiment_creator/project.html', {
        'project': project
    })


@user_passes_test(university_email_or_staff_check)
def experiment_dash(request, year, course_slug, term, group_id, slug):
    """Experiment dashboard."""
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    ex_group = get_object_or_404(CourseGroup, course=course, id=group_id)
    experiment = get_object_or_404(Experiment, slug=slug, group=ex_group)

    stimuli_folder, _ = experiment.folder.children.get_or_create(name='Stimuli')

    try:
        runner_experiment = RunnerExperiment.objects.get(name=experiment.slug)
    except ObjectDoesNotExist:
        runner_experiment = None

    try:
        design = experiment.stimulidesign
    except ObjectDoesNotExist:
        design = None

    try:
        stimuli = experiment.experimentstimuli
    except ObjectDoesNotExist:
        stimuli = None

    return render(request, 'experiment_creator/experiment.html', {
        'course': course,
        'group': ex_group,
        'experiment': experiment,
        'runner_experiment': runner_experiment,
        'design': design,
        'stimuli': stimuli,
        'folders': {
            'stimuli': stimuli_folder,
        }
    })


@user_passes_test(university_email_or_staff_check)
def stimuli_upload(request, year, course_slug, term, group_id, slug):
    """Upload project stimuli."""
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    ex_group = get_object_or_404(CourseGroup, course=course, id=group_id)
    experiment = get_object_or_404(Experiment, slug=slug, group=ex_group)
    stimuli, _ = ExperimentStimuli.objects.get_or_create(group=ex_group, experiment=experiment)

    if ex_group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    if not stimuli.folder:
        stimuli.folder, _ = experiment.folder.children.get_or_create(name='Stimuli')

    file_base = '{}Stimuli-v{}'.format(ex_group.name, stimuli.folder.file_count + 1)

    if request.method == 'POST':
        # Create a form instance from POST data.
        form = ExcelForm(request.POST, request.FILES)
        if form.is_valid():
            # Save a new ExcelDocument object from the form's data, but don't commit
            # because we need to add a bunch of foreign keys
            new_stimuli = form.save(commit=False)
            new_stimuli.folder = stimuli.folder
            new_stimuli.file = request.FILES['file']
            new_stimuli.owner = request.user
            new_stimuli.name = '{}.{}'.format(file_base, new_stimuli.file.instance.extension)
            # Set the name on the filesystem to the same name as above, but with the existing Filer path
            file_path = os.path.split(new_stimuli.file.name)[0]
            new_stimuli.file.name = os.path.join(file_path, new_stimuli.name)
            new_stimuli.save()

            # Save the many-to-many data for the form. Required if you commit=False
            # and modify and save model
            form.save_m2m()

            stimuli.validate()

            # Once a new file has been uploaded, make sure the status is
            # 'Uploaded' not something like 'Requested Approval'
            if stimuli.folder.file_count > 0:
                stimuli.status = 'RUP'
            else:
                stimuli.status = 'UPL'
            stimuli.save()

            messages.add_message(request, messages.SUCCESS, 'Stimuli uploaded successfully.')
            action.send(request.user, verb='uploaded stimuli document',
                        action_object=stimuli,
                        target=ex_group)
            subject = '{} from {} uploaded a stimuli file'.format(request.user.get_full_name(), ex_group.name)
            message = "You can review the stimuli at {}".format(
                request.build_absolute_uri(reverse('review_stimuli', kwargs={'stimuli_id': stimuli.id})))
            email_course_staff(course.id, subject, message)

            return HttpResponseRedirect(reverse('experiment_dash',
                                        kwargs={'year': course.year,
                                                'term': course.term,
                                                'course_slug': course.slug,
                                                'group_id': ex_group.id,
                                                'slug': experiment.slug}))
    else:
        form = ExcelForm()

    return render(request, 'experiment_creator/stimuli_upload.html', {
        'course': course,
        'group': ex_group,
        'experiment': experiment,
        'form': form,
    })


@user_passes_test(university_email_or_staff_check)
def request_design_approval(request, experiment_id):
    """Request approval for an experiment design."""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    design = get_object_or_404(StimuliDesign, experiment=experiment, group=experiment.group)

    if experiment.group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    design.status = 'REQ'
    design.save()

    messages.add_message(request, messages.INFO, 'Design Approval Requested')
    action.send(request.user, verb='requested design approval',
                action_object=design,
                target=experiment.group)

    subject = '{} from {} requested approval of experiment design'.format(request.user.get_full_name(), experiment.group.name)
    message = "You can review the experiment design at {}".format(
        request.build_absolute_uri(reverse('review_design', kwargs={'design_id': design.id})))
    email_course_staff(experiment.group.course.id, subject, message)

    return HttpResponseRedirect(reverse('experiment_dash',
                                kwargs={'year': experiment.group.course.year,
                                        'term': experiment.group.course.term,
                                        'course_slug': experiment.group.course.slug,
                                        'group_id': experiment.group.id,
                                        'slug': experiment.slug}))


@user_passes_test(university_email_or_staff_check)
def request_stimuli_approval(request, year, course_slug, term, group_id, slug):
    """Request approval for an ExperimentGroup's Stimuli."""
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    ex_group = get_object_or_404(CourseGroup, course=course, id=group_id)
    experiment = get_object_or_404(Experiment, slug=slug, group=ex_group)

    if ex_group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    stimuli = get_object_or_404(ExperimentStimuli, group=ex_group, experiment=experiment)
    stimuli.status = 'REQ'
    stimuli.save()

    messages.add_message(request, messages.INFO, 'Requested stimuli approval')
    action.send(request.user, verb='requested stimuli approval',
                action_object=stimuli,
                target=ex_group)

    subject = '{} from {} requested approval of experiment stimuli'.format(request.user.get_full_name(), ex_group.name)
    message = "You can review the experiment stimuli at {}".format(
        request.build_absolute_uri(reverse('review_stimuli', kwargs={'stimuli_id': stimuli.id})))
    email_course_staff(course.id, subject, message)

    return HttpResponseRedirect(reverse('experiment_dash',
                                kwargs={'year': course.year,
                                        'term': course.term,
                                        'course_slug': course.slug,
                                        'group_id': ex_group.id,
                                        'slug': experiment.slug}))


@user_passes_test(university_email_or_staff_check)
def request_experiment_approval(request, experiment_id):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    if experiment.group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    experiment.status = 'STS'
    experiment.save()
    action.send(request.user, verb='requested experiment staff testing', action_object=experiment)
    messages.add_message(request, messages.INFO, 'Staff Testing of Experiment Requested')

    subject = '{} from {} requested staff testing of experiment'.format(request.user.get_full_name(), experiment.group.name)
    message = "You can review the experiment at {}".format(
        request.build_absolute_uri(reverse('review_experiment', kwargs={'experiment_id': experiment.id})))
    email_course_staff(experiment.group.course.id, subject, message)

    return HttpResponseRedirect(reverse('experiment_dash', kwargs={'year': experiment.group.course.year,
                                                                   'term': experiment.group.course.term,
                                                                   'course_slug': experiment.group.course.slug,
                                                                   'group_id': experiment.group.id,
                                                                   'slug': experiment.slug}))


@user_passes_test(university_email_or_staff_check)
def withdraw_from_testing(request, experiment_id):
    experiment = get_object_or_404(Experiment, id=experiment_id)

    if experiment.group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    # Reset experiment status to editing stimuli
    experiment.status = 'STM'
    experiment.save()

    # Reset stimuli status to revision requested
    stimuli = experiment.experimentstimuli
    stimuli.status = 'REV'
    stimuli.save()

    action.send(request.user, verb='withdrew experiment from testing', action_object=experiment)
    messages.add_message(request, messages.WARNING, 'Experiment Withdrawn from Testing')

    return HttpResponseRedirect(reverse('experiment_dash', kwargs={'year': experiment.group.course.year,
                                                                   'term': experiment.group.course.term,
                                                                   'course_slug': experiment.group.course.slug,
                                                                   'group_id': experiment.group.id,
                                                                   'slug': experiment.slug}))


@user_passes_test(university_email_or_staff_check)
def create_design(request, year, course_slug, term, group_id, slug):
    """Create experiment design form."""
    course = get_object_or_404(Course, year=year, term=term, slug=course_slug)
    ex_group = get_object_or_404(CourseGroup, course=course, id=group_id)
    experiment = get_object_or_404(Experiment, slug=slug, group=ex_group)

    if ex_group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    return render(request, 'experiment_creator/create_design.html', {
        'course': course,
        'group': ex_group,
        'experiment': experiment,
    })


@user_passes_test(university_email_or_staff_check)
def get_set_design(request, experiment_id):
    """JSON method for getting or setting experiment design."""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    design, _ = StimuliDesign.objects.get_or_create(experiment=experiment, group=experiment.group)

    if experiment.group not in request.user.course_groups.all() and not request.user.is_staff:
        if request.is_ajax:
            return JsonResponse({'message': 'Forbidden: Not a member of this group'}, status=403)
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    if request.is_ajax():
        if request.method == 'POST':
            proposal = request.POST.get('proposal')
            jsondesign = request.POST.get('design')
            if not jsondesign:
                return JsonResponse({'message': "Missing required 'design' JSON object"}, status=400)
            design.proposal = proposal
            design.design.update(json.loads(jsondesign))
            if design.status in ('APP', 'REQ', 'REV'):
                design.status = 'RUP'
            design.save()
            return JsonResponse({'design': design.design, 'proposal': design.proposal}, safe=False)
        elif request.method == 'GET':
            if design.design['exname'] == '':
                design.design['exname'] = experiment.name.strip().replace(' ', '.')
            return JsonResponse({'design': design.design, 'proposal': design.proposal}, safe=False)
    else:
        return HttpResponseBadRequest('Request must be an XHR')


@user_passes_test(university_email_or_staff_check)
def download_excel_template(request, experiment_id):
    """Download Excel template based on current experiment design."""
    experiment = get_object_or_404(Experiment, id=experiment_id)
    design = get_object_or_404(StimuliDesign, experiment=experiment, group=experiment.group)

    if experiment.group not in request.user.course_groups.all() and not request.user.is_staff:
        messages.add_message(request, messages.ERROR, 'Not a member of this group')
        return HttpResponseForbidden('Not a member of this group')

    manips = [m['items'] for m in design.design['manipulations']]
    try:
        itemcount = int(design.design['trialcount'])
    except ValueError:
        itemcount = 0
    try:
        fillercount = int(design.design['fillercount'])
    except ValueError:
        fillercount = 0
    exname = design.design['exname']

    wb = create_xlsx_template(exname, itemcount, fillercount, manips)

    response = HttpResponse(save_virtual_workbook(wb), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="{}-{}-template.xlsx"'.format(experiment.group.name, experiment.slug)
    return response


@user_passes_test(university_email_or_staff_check)
def preview_stimuli(request, stimuli_id):
    """Preview how stimuli will look before finalizing."""
    stimuli = get_object_or_404(ExperimentStimuli, id=stimuli_id)

    return render(request, 'experiment_creator/stimuli_preview.html', {
        'stimuli': stimuli,
        'parsed_stims': stimuli.generate_parsed()
    })
