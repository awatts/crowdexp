# CrowdExp
CrowdExp (Crowd Experiments) is a web-based system for students to create experiments and run them on Amazon
[Mechanical Turk](https://www.mturk.com/mturk/welcome).

It was created for Brain and Cognitive Science 152 (Language & Psycholinguistics) at the University of Rochester.

The current version only allows creating Self Paced Reading experiments using the
[FlexSPR](https://bitbucket.org/hlplab/flexsprapplet) Flash applet developed by Hal Tily and modified by our lab. Future
versions will hopefully be more flexible and allow a variety of different experiment types.

Students can:

  - create and join groups
  - create stimuli designs
  - download stimuli file templates based on their designs
  - upload stimuli files
  - view a demo version of their experiment (with all trial lists) for testing
  - request to run an experiment

Staff can:

  - create and manage groups for students
  - approve or reject stimuli designs and stimuli files
  - approve experiments to run

Administrators can:

  - do anything staff can
  - run experiments on MTurk

## Getting Started
TBD

### Mininum Python version
Requires Python >= 3.5, because of PEP 484 type hinting.

### External dependencies

#### BabelJS to convert ES6 -> old fashioned JavaScript
Used by Django Pipeline

```bash
npm install --save-dev babel-cli babel-preset-es2015
```

## Deployment
TBD

## Authors
 - Andrew Watts <awatts2@ur.rochester.edu>

## Contributing
  - Pull requests are greatly appreciated
  - Feel free to email the author(s) with suggestions, comments, or questions

## License
This project is licensed under the [BSD 2-clause "Simplified" License](https://choosealicense.com/licenses/bsd-2-clause/) -
see the [LICENSE](LICENSE) file for details


## Acknowledgements
This project was partially funded by a [Discover Grant](https://www.rochester.edu/college/ugresearch/discover.html) from
the University of Rochester to T. Florian Jaeger and Olga Nikolayeva in 2015. The purpose of the grant is to get
undergraduates involved in research.

This project is partially funded by NSF IIS-1150028: CAREER: _Communicative Efficiency and
Adaptiveness in the Ideal Speaker_ (2012-2017) to T. Florian Jaeger
