# Required Django plugins

## Authentication
  - [Django AllAuth](http://django-allauth.readthedocs.org/en/latest/index.html)
      - Advantage: works correctly with Google
      - [Tutorial](http://www.sarahhagstrom.com/2013/09/the-missing-django-allauth-tutorial/)

## File management
  - [Django Filer](https://django-filer.readthedocs.org/en/latest/)
    - [Extend](https://django-filer.readthedocs.org/en/latest/extending_filer.html) with types of files we will be uploading

## Notifications
  - [Django Activity Stream](https://github.com/justquick/django-activity-stream)

## Asset management
 - [Django Pipeline](https://django-pipeline.readthedocs.io/en/latest/)


# Unused options (could change)

## Authentication
  - [~~Python Social Auth~~](https://python-social-auth.readthedocs.org/en/latest/intro.html)
      - Advantage: has whitelisting ability
  - [~~Django OAuth Toolkit~~](https://django-oauth-toolkit.readthedocs.org/en/latest/)

## Passwords
  - [Django zxcvbn](https://github.com/Pawamoy/django-zxcvbn-password) - make users use strong passwords

## Notifications
  - [~~SwampDragon~~](http://swampdragon.net/)
  
## Run as user
- [Django Hijack](http://django-hijack.readthedocs.io/en/stable/)

# Stimuli constraints

1. Check stimuli/data files for weird characters, i.e. no ',', ';', '\t' in the sentences or questions
2. Force there to be one question per item
3. Format checking of stimuli files:
  - For each manipulation there is one field in the condition name:
    For a two manipulation experiment with 2 levels per manipulation (aka 2x2), condition names should be in the format A.B so that we can create the analysis design.
    e.g. 2x2 high vs low & fruit vs animal, condition names are:
      high.fruit, high.animal, low.fruit, low.animal

  - Question answer within an item is always the same
  - non-filler & non-practice sentences should have one item per condition

  - non-filler & non-practice sentences have the same named regions in all items
  - non-filler & non-practice sentence regions must be in the same order across items

  - punctuation of a word must be within a region (or don't allow region names to end in punctuations or be followed by a space then punctuation)
    - OK: {word1 word2,}@region1
    - NO: {word1 word2}@region1,
    - NO: {word1 word2}@region1 ,

# Misc notes

  - After they fill out info about design, generate an Excel file they can download that has 'Experiment','ItemName','Condition' columns filled in. Lock those columns.

