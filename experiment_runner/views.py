from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from random import randrange

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST, require_GET

from .models import Experiment
from .utils.list_assignment import get_or_create_worker_assignment


@staff_member_required
def experiments(request):
    """Displays list of experiments in the DB"""
    return render(request, 'experiment_runner/experiments.html', {'experiments': Experiment.objects.all()})


@staff_member_required
def experiment_detail(request, experiment):
    e = get_object_or_404(Experiment, name=experiment)
    return render(request, 'experiment_runner/experiment_detail.html', {'experiment': e})


@require_GET
def spr_experiment(request, experiment):
    """Run a given SPR experiment. Hits the DB for worker, assignment, etc."""

    get_object_or_404(Experiment, name=experiment)

    required_keys = {'assignmentId', }
    key_error_msg = 'Missing parameter: {0}. Required keys: {1}'

    assignmentid = request.GET.get('assignmentId', None)
    workerid = request.GET.get('workerId', None)
    dest = request.GET.get('dest', None)

    in_preview = assignmentid == 'ASSIGNMENT_ID_NOT_AVAILABLE'

    missing_keys = {k for k in required_keys if k not in request.GET}

    condition = None
    if not in_preview:
        if workerid:
            assignment = get_or_create_worker_assignment(
                                     experiment,
                                     workerid,
                                     assignmentid)
            condition = "{}/{}".format(experiment, assignment.triallist.name)
        else:
            missing_keys.add('workerId')

    if missing_keys:
        return HttpResponseBadRequest(content=key_error_msg.format(missing_keys, required_keys))

    return render(request, 'experiment_runner/bcs152.html', {
        'experiment': experiment,
        'formtype': dest if dest in ('mturk', 'sandbox') else None,
        'preview': in_preview,
        'condition': condition,
        'amz': {
            'assignmentId': assignmentid,
        }
    })


@login_required
def spr_demo(request, experiment, region=None, listname=None, listid=None):
    """Demo version of a given SPR experiment. Doesn't hit DB for anything."""
    amz = {
        'assignmentId': 'NA',
        'region': region
    }
    e = get_object_or_404(Experiment, name=experiment)
    if not listid:
        randlist = e.lists.all()[randrange(0, e.lists.count())]
        listid = randlist.name
    condition = "{}/{}".format(e.name, listid)
    return render(request, 'experiment_runner/bcs152.html', {'experiment': e.name, 'condition': condition, 'amz': amz, 'demo': True})


@require_POST
def response_repeater(request):
    """
    Take all the keys and values in a request object and print them in a table
    """
    return render(request, 'experiment_runner/echo.html', formdata=request.POST)
