from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
from math import ceil

from boto import sqs
from dateutil.parser import parse
from django.conf import settings

from ..models import NotificationEvent, HITId, HITTypeId, Assignment

AWS_SETTINGS = getattr(settings, 'AWS', {
    'SQS_REGION': '',
    'SQS_QUEUE': '',
    'AWS_ACCESS_KEY': '',
    'AWS_SECRET_KEY': ''
})

SQS_REGION = AWS_SETTINGS['SQS_REGION']
SQS_QUEUE = AWS_SETTINGS['SQS_QUEUE']
AWS_ACCESS_KEY = AWS_SETTINGS['AWS_ACCESS_KEY']
AWS_SECRET_KEY = AWS_SETTINGS['AWS_SECRET_KEY']

assignment_map = {
    'AssignmentAccepted': 'InProcess',
    'AssignmentAbandoned': 'Abandoned',
    'AssignmentReturned': 'Returned',
    'AssignmentSubmitted': 'Submitted'
}


def process_notifications() -> None:
    """
    Fetch all notification from the SQS queue, create NotificationEvents for each
    and update the state of any affected Assignments
    """
    conn = sqs.connect_to_region(SQS_REGION, aws_access_key_id=AWS_ACCESS_KEY, aws_secret_access_key=AWS_SECRET_KEY)
    # FIXME: Should do something more helpful than silently fail and return
    if conn is None:
        return
    my_queue = conn.get_queue(SQS_QUEUE)
    if my_queue is None:
        return

    qcount = my_queue.count()
    if qcount > 0:
        #  MaxNumberOfMessages must be between 1 and 10, so loop if > 10
        messages = []
        for _ in range(int(ceil(qcount / 10))):
            messages.extend(my_queue.get_messages(10))

            # FIXME: some exception handling should happen in here
        for msg in messages:
            mdict = json.loads(msg.get_body())
            docid = mdict['EventDocId']
            for e in mdict['Events']:
                hittypeid, _ = HITTypeId.objects.get_or_create(hittypeid=e['HITTypeId'])
                hitid, _ = HITId.objects.get_or_create(hitid=e['HITId'], hittypeid=hittypeid)
                note_event, _ = NotificationEvent.objects.get_or_create(event_type=e['EventType'],
                                                                        eventdocid=docid,
                                                                        hittypeid=hittypeid,
                                                                        hitid=hitid,
                                                                        event_time=parse(e['EventTimestamp'])
                                                                        )
                assignment = None
                if e['EventType'] in ('AssignmentAccepted', 'AssignmentAbandoned',
                                      'AssignmentReturned', 'AssignmentSubmitted'):
                    assignment, _ = Assignment.objects.get_or_create(assignmentid=e['AssignmentId'])
                    if assignment.state != assignment_map[e['EventType']]:
                        # notifications can come out of order; don't allow update
                        # to 'Accepted' if already 'Returned', 'Abandoned', or 'Submitted'
                        if assignment.state == 'InProcess':
                            assignment.state = assignment_map[e['EventType']]
                            assignment.save()
                            # Some event types are HIT based, not assignment based
                # Only add the assignment to the NotificationEvent is new
                # and has no assignment connected to it
                if assignment and note_event.assignment is None:
                    note_event.assignment = assignment
                    note_event.save()

        while len(messages) > 0:
            my_queue.delete_message_batch(messages[:10])
            messages[:10] = []

# example SQS notification with event
# {u'CustomerId': u'A29979FPDJ98R8',
#  u'EventDocId': u'2d63dbfd28a75a32325390ed2acd193dbe7cdfcd',
#  u'EventDocVersion': u'2006-05-05',
#  u'Events': [{u'AssignmentId': u'1234567890123456789012345678901234567890',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'},
#              {u'AssignmentId': u'1234567890123456789012345678900987654321',
#               u'EventTimestamp': u'2014-11-19T18:58:16Z',
#               u'EventType': u'AssignmentAbandoned',
#               u'HITId': u'12345678901234567890',
#               u'HITTypeId': u'09876543210987654321'}]}
