from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from collections import namedtuple, Counter
from random import choice
from typing import Optional, List

from django.shortcuts import get_object_or_404

from .notifications import process_notifications
from ..models import (
    Worker,
    Assignment,
    TrialList,
    Experiment
)

ListCount = namedtuple('ListCount', ['ListId', 'Count'])


def get_or_create_worker_assignment(experiment: str, workerid: str, assignmentid: str, chooselist: str='random', **kwargs) -> Assignment:
    """
    Looks for a Worker with a given MTurk workerid in the database, creating one
    if necessary. Then looks for an Assigment with a given assignmentid in the
    database, creating one if necessary, and adding the worker and a list by the
    selected method.
    :param experiment: Name of the experiment
    :param workerid: WorkerId to find or create
    :param assignmentid: AssignmentId to find or create
    :param chooselist:  How to choose a list for the worker: 'random' (default), 'filtered', or 'static'
    :param kwargs: Keyword arguments to pass on to list choosing functions
    """
    worker, _ = Worker.objects.get_or_create(workerid=workerid)
    expt = get_object_or_404(Experiment, name=experiment)

    # This seems like the best point to check the notification queue and
    # make sure assignment statuses are up to date
    # TODO: instead, set up a timer to check notifications every 30 seconds until a HITReviewable
    # event with the right HITTypeId comes up. Maybe add a refcounter so if multiple experiments
    # are running, we don't cancel while others need to be pumping the queue. Alternately: Add a
    # property to HITTypeIds for whether they are active, set to true on creation and on a HITExtended
    # event and false on a HITReviewable or HITExpired event
    process_notifications()

    # If a a notification event created an assignment, it might not have worker and experiment set
    # so just search / create on assignmentid
    assignment, _ = Assignment.objects.get_or_create(assignmentid=assignmentid)

    if not assignment.worker:
        assignment.worker = worker
    if not assignment.experiment:
        assignment.experiment = expt
    assignment.save()

    if not assignment.triallist:
        if chooselist == 'random':
            assignment.triallist = random_lowest_list(experiment)
        elif chooselist == 'filtered':
            assignment.triallist = random_lowest_list(experiment, kwargs['lsubset'])
        elif chooselist == 'static':
            assignment.triallist = static_list(experiment, kwargs['listname'])
    assignment.save()

    return assignment


def random_lowest_list(experiment: str, listsubset: Optional[List[str]]=None) -> TrialList:
    """
    Randomly select a trial list from all available lists.

    If listsubset is a sequence of list names, restrict to those lists

    :param experiment: Name of the experiment
    :param listsubset: Sequence of list names
    :return a TrialList
    """
    expt = Experiment.objects.get(name=experiment)

    # restrict to assignments with lists in the specified subset if given
    expt_lists = TrialList.objects.filter(experiment=expt)

    if listsubset:
        expt_lists = expt_lists.filter(name__in=listsubset)

    list_counts = Counter(
        {x.id: len(x.assignments.filter(state__in={'InProcess', 'Submitted', 'Approved'}))
         for x in expt_lists.all()})

    if list_counts.most_common():
        lowcount = list_counts.most_common(len(list_counts))[-1][1]  # count element of last list element
        lowlist = choice([k for k, v in list_counts.items() if v <= lowcount])
        return TrialList.objects.get(id=lowlist)
    else:  # on the first assignment for an experiment pick any list
        return random_list(experiment)


def random_list(experiment: str) -> TrialList:
    """
    :param experiment:
    :return: Any TrialList from experiment

    Randomly selects any TrialList from a given Experiment
    """
    expt = Experiment.objects.get(name=experiment)
    all_lists = TrialList.objects.filter(experiment=expt).all()
    return choice(all_lists)


def static_list(experiment: str, listnameid: str) -> TrialList:
    """
    :param experiment:
    :param listnameid:
    :return: TrialList object whose name matches listnameid within experiment
    """
    expt = Experiment.objects.get(name=experiment)
    filtered_list = TrialList.objects.filter(experiment=expt)
    if isinstance(listnameid, int):
        return filtered_list.get(id=listnameid)
    else:
        return filtered_list.get(name=listnameid)
