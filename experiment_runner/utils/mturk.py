from collections import Sequence
from itertools import chain
from math import ceil
import re
from typing import List, Dict
from uuid import uuid4

from boto.mturk.connection import MTurkConnection, MTurkRequestError, Assignment, HIT
from numpy import mean
import pandas as pd


def manage_url(hit: HIT, sandbox: bool=False) -> str:
    """URL for managing a given HITId"""
    mturk_website = 'requestersandbox.mturk.com' if sandbox else 'requester.mturk.com'
    return 'https://{}/mturk/manageHIT?HITId={}'.format(
        mturk_website, hit.HITId)


def download_assignments(mtc: MTurkConnection, hitids: List[str]) -> List[Assignment]:
    """Download assignments for a list of HITIds"""
    assignments = []
    for h in hitids:
        assignments = mtc.get_assignments(h)  #, response_groups=['Request', 'Minimal', 'AssignmentFeedback', 'HITDetail', 'HITQuestion'])
        total_results = int(assignments.TotalNumResults)
        if total_results > 10:
            pages = int(ceil(total_results / 10))
            for i in range(1, pages):
                assignments += mtc.get_assignments(h, page_number=i+1)
    return assignments


def get_hit_info(mtc: MTurkConnection, hitids: List[str]) -> Dict[str, HIT]:
    return {h: mtc.get_hit(h, response_groups=["Request", "Minimal", "HITDetail", "HITAssignmentSummary"])[0] for h in hitids}


def process_assignment(assignment: Assignment, hit: HIT, sandbox: bool=False) -> pd.Series:
    """Given an Assignment and HIT object, return a Pandas Series with results data"""
    row = {
        'assignmentid': assignment.AssignmentId,
        'assignmentstatus': assignment.AssignmentStatus,
        'autoapprovaltime': assignment.AutoApprovalTime,
        'hitid': assignment.HITId,
        'assignmentsubmittime': assignment.SubmitTime,
        'workerid': assignment.WorkerId,

        'hittypeid': hit.HITTypeId,
        'title': hit.Title,
        'description': hit.Description,
        'keywords': hit.Keywords,
        'reward': hit.FormattedPrice,
        'creationtime': hit.CreationTime,
        'assignments': hit.MaxAssignments,
        'numavailable': hit.NumberOfAssignmentsAvailable,
        'numpending': hit.NumberOfAssignmentsPending,
        'numcomplete': hit.NumberOfAssignmentsCompleted,
        'hitstatus': hit.HITStatus,
        'reviewstatus': hit.HITReviewStatus,
        'assignmentduration': hit.AssignmentDurationInSeconds,
        'autoapprovaldelay': hit.AutoApprovalDelayInSeconds,
        'hitlifetime': hit.Expiration,

        #  'reject' is is used to mark a HIT for rejection with the AWS CLT
        'reject': '',
        'assignmentaccepttime': '',
        'assignmentrejecttime': '',
        'assignmentapprovaltime': '',
        'deadline': '',
        'feedback': '',
        'annotation': '',
        'viewhit': manage_url(hit, sandbox)
    }

    optional = {
        'AcceptTime': 'assignmentaccepttime',
        'RejectTime': 'assignmentrejecttime',
        'Deadline': 'deadline',
        'RequesterFeedback': 'feedback',
        'ApprovalTime': 'assignmentapprovaltime',
    }
    for k in (set(optional.keys()) & set(assignment.__dict__.keys())):
        row[optional[k]] = assignment.__dict__[k]

    for a in assignment.answers:
        for q in a:
            row['Answer.{}'.format(q.qid)] = ','.join(q.fields)

    if 'RequesterAnnotation' in hit.__dict__.keys():
        row['annotation'] = hit.RequesterAnnotation

    return pd.Series(row)


def get_results(hitids: List[str], sandbox: bool=False) -> pd.DataFrame:
    """Results"""
    host = 'mechanicalturk.sandbox.amazonaws.com' if sandbox else 'mechanicalturk.amazonaws.com'
    mtc = MTurkConnection(host=host)

    hit_info = get_hit_info(mtc, hitids)

    assignments = download_assignments(mtc, hitids)

    return pd.DataFrame([process_assignment(a, hit_info[a.HITId], sandbox) for a in assignments])


def pipesplit(c):
    """Split a column on the pipe character"""
    try:
        return [x.strip() for x in c.split('|') if x.strip() != '']
    except:
        return c


def convert_results(resultsfile: str, regionfile: str):
    """Load results tab delimited file from MTurk and output a tab delim file in analyzable format"""
    df = pd.read_csv(resultsfile, sep='\t')
    regions = pd.read_pickle(regionfile)

    workers = df['workerid'].unique().tolist()

    newids = {w: uuid4().hex for w in workers}  # TODO: convert to Series and use to_csv to dump mapping to file
    for k in newids.keys():
        df.loc[df['workerid'] == k, 'UUID'] = newids[k]

    tre = re.compile('Answer.trial([1-9]\d*)')
    wre = re.compile('Answer.words([1-9]\d*)')
    rre = re.compile('Answer.rts([1-9]\d*)')

    all_columns = df.columns.tolist()
    trial_columns = {int(tre.match(c).groups()[0]): c for c in all_columns if tre.match(c)}
    word_columns = {int(wre.match(c).groups()[0]): c for c in all_columns if wre.match(c)}
    rts_columns = {int(rre.match(c).groups()[0]): c for c in all_columns if rre.match(c)}
    spr_columns = list(trial_columns.values()) + list(word_columns.values()) + list(rts_columns.values())

    for col in spr_columns:
        df[col] = df[col].apply(pipesplit)

    # Just select the columns we care about for output
    df = df[spr_columns + ['UUID', ]]

    reg_dict = regions.to_dict()
    reg_items = reg_dict['ItemName']
    region_map = {reg_items[k]: reg_dict['Regions'][k] for k in reg_items.keys()}

    trialdfs = []
    for row in df.iterrows():
        for trial_num in trial_columns.keys():
            r = row[1]  # iterrows gives you a (int, pd.Series) tuple
            expname, cond, item, resp = r[trial_columns[trial_num]]
            raw_resp = list(resp)
            correct = ', '.join([{'T': 'Correct', 'F': 'Incorrect'}[x] for x in raw_resp])
            qscore = mean([{'T': 100, 'F': 0}[x] for x in raw_resp])
            curr_words = r[word_columns[trial_num]]
            curr_rts = r[rts_columns[trial_num]]

            word_range = range(len(curr_words))
            indices = [x for x in word_range]

            if type(curr_rts) is not Sequence:
                curr_rts = [0 for _ in range(len(curr_words) + 1)]

            # The wacky list(chain(*nestedlists)) thing is a fast way to flatten nested lists
            region_list = list(chain(*[[r.tag for _ in range(r.stop - r.start)] for r in region_map[trial_num]]))

            # TODO: validate length of words vs rts and log if incorrect.
            # remember 1st RT is time until press to see 1st word
            tdf = pd.DataFrame({
                'Exp': pd.Series([expname for _ in word_range], index=indices),
                'Cond': pd.Series([cond for _ in word_range], index=indices),
                'Item': pd.Series([item for _ in word_range], index=indices),
                'Subj': pd.Series([r['UUID'] for _ in word_range], index=indices),
                'Trial': pd.Series([trial_num for _ in word_range], index=indices),
                'WordPos': pd.Series(indices, index=indices),
                'Word': pd.Series([w for w in curr_words], index=indices),
                'RegionName': pd.Series(region_list, index=indices),
                'RawRT': pd.Series([r for r in curr_rts[1:len(curr_words) + 1]], index=indices),
                'RawQuestion': pd.Series([correct for _ in word_range], index=indices),
                'Question': pd.Series([qscore for _ in word_range], index=indices),
            })
            trialdfs.append(tdf)
    outdf = pd.concat(trialdfs, ignore_index=True)
    outdf.to_csv()  # TODO: figure out where to write the file




