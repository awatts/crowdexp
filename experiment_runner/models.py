from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from django.db import models
from django_mysql.models import EnumField


class Worker(models.Model):
    """
    Representation of an mturk worker and what list they were assigned to
    """
    workerid = models.CharField(max_length=32, unique=True, blank=True, null=True)

    def __repr__(self):
        return '<Worker: "{}">'.format(self.workerid)

    class Meta:
        managed = False
        db_table = 'worker'

# FIXME: once old setup is working, migrate this
# class Protocol(models.Model):
#     """
#     RSRB protocol.
#     """
#     name = models.CharField(128, unique=True)
#     number = models.CharField(24, unique=True)


class Experiment(models.Model):
    """
    Representation of an experiment
    """
    name = models.CharField(max_length=128, unique=True, blank=True, null=True)
    # FIXME: once the old setup is working, migrate this
    # protocol = models.ForeignKey(Protocol, related_name='experiments')

    def __repr__(self):
        return 'Experiment: "{}"'.format(self.name)

    class Meta:
        managed = False
        db_table = 'experiment'


class TrialList(models.Model):
    """
    Which list workers are on
    """

    name = models.CharField(max_length=32, blank=True, null=True)
    number = models.PositiveIntegerField(blank=True, null=True)
    experiment = models.ForeignKey(Experiment, related_name='lists', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return '<TrialList: "{}::{}">'.format(self.experiment, self.name)

    class Meta:
        managed = False
        unique_together = (('experiment', 'name'),)
        db_table = 'triallist'


class Assignment(models.Model):
    """
    A MTurk HIT assignment.
    """
    assignmentid = models.CharField(max_length=32, unique=True, blank=True, null=True)
    state = EnumField(choices=('InProcess', 'Submitted', 'Abandoned', 'Returned', 'Rejected', 'Approved'),
                      default='InProcess')

    list = models.ForeignKey(TrialList, related_name='assignments', on_delete=models.DO_NOTHING)
    worker = models.ForeignKey(Worker, related_name='assignments', on_delete=models.DO_NOTHING)
    experiment = models.ForeignKey(Experiment, related_name='assignments', on_delete=models.DO_NOTHING)

    def __repr__(self):
        try:
            return 'Assignment: "{}" TrialList: {}::{}'.format(self.assignmentid, self.experiment.name, self.triallist.name)
        except AttributeError:
            return 'Assignment: "{}"'.format(self.assignmentid)

    class Meta:
        managed = False
        db_table = 'assignment'


class HITTypeId(models.Model):
    """
    An Amazon Mechanical Turk HITTypeId
    """
    hittypeid = models.CharField(max_length=32, blank=True, null=True)

    def __repr__(self):
        return 'HITTypeId: "{}"'.format(self.hittypeid)

    class Meta:
        managed = False
        db_table = 'hittypeid'


class HITId(models.Model):
    """
    An Amazon Mechanical Turk HITId
    """
    hitid = models.CharField(max_length=32, blank=True, null=True)
    hittypeid = models.ForeignKey(HITTypeId, related_name='HITS', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return 'HITId: "{}"'.format(self.hitid)

    class Meta:
        managed = False
        db_table = 'hitid'


class NotificationEvent(models.Model):
    """
    An Amazon Mechanical Turk notification event.
    """
    event_type = EnumField(choices=('AssignmentAccepted', 'AssignmentAbandoned',
                                    'AssignmentReturned', 'AssignmentSubmitted',
                                    'HITReviewable', 'HITExpired', 'Ping'))
    event_time = models.DateTimeField(blank=True, null=True)
    eventdocid = models.CharField(max_length=48, blank=True, null=True)
    assignment = models.ForeignKey(Assignment, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)
    hitid = models.ForeignKey(HITId, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)
    hittypeid = models.ForeignKey(HITTypeId, related_name='events', on_delete=models.DO_NOTHING, blank=True, null=True)

    def __repr__(self):
        return 'Event: "{}::{}"'.format(self.event_type, self.event_time)

    class Meta:
        managed = False
        db_table = 'notificationevent'
