/* Mostly comes from the Timer demo on the React homepage on 2016-11-16 */
class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {secondsElapsed: 0, minutes: 0, seconds: 0};
    }

    tick() {
        this.setState((prevState) => ({
            secondsElapsed: prevState.secondsElapsed + 1,
            minutes: Math.floor(this.state.secondsElapsed / 60),
            seconds: this.state.secondsElapsed % 60
        }));
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div>Time Elapsed: {this.state.minutes} minutes, {this.state.seconds} seconds</div>
    );
    }
}

ReactDOM.render(<Timer />, document.getElementById('timernode'));
