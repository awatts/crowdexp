function register(field, answer) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log('Registering: ' + field + ' with value: ' + answer);
    }
    $('<input>').attr({
        type: 'hidden',
        id: field,
        name: field
    }).val(answer).appendTo('form');
    //document.getElementById(field).value = answer;
    return true;
}

function onExperimentEnd() {
    $("#comment").show(function(){$('#commentarea').focus();});
    $("#submit").show(function() {
        $(this).removeAttr('disabled');
    });
}

function log(args) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log(args);
    }
}

var flashref = null;
function flashLoadedCB(e) {
    if (e.success) {
        flashref = e.ref;
    } else {
        $('#flashcontent').html('Failed to load flash content.');
    }
}

$(document).ready(function() {
    $('input[name="browserid"]').val(navigator.userAgent);
    $('input[name="flashversion"]').val(swfobject.ua.pv.join("."));
    var params = {allowScriptAccess: "always", menu: "false", bgcolor:"#ffffff", quality: "high"};
    $('button#endinstr').on('click', function(){
        $('#instructions').hide(function() {
            swfobject.embedSWF(swfurl,
                "flashcontent", "950", "600", "12.0.0", null,
                flashvars, params, {}, function(e) {flashLoadedCB(e);});
        });
    });
});
